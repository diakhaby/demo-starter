package com.transfert.electronicwallet.repositories;

import com.transfert.electronicwallet.domain.entities.AgencyEntity;
import com.transfert.electronicwallet.domain.entities.TransferEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 20/02/2024
 */
@Repository
public interface TransferRepository extends JpaRepository<TransferEntity, String>,
        PagingAndSortingRepository<TransferEntity, String> {
    Page<TransferEntity> findByAgencysender(AgencyEntity agencysender, Pageable pageable);

    Page<TransferEntity> findByAgencyreceiver(AgencyEntity agencyreceiver, Pageable pageable);
}
