package com.transfert.electronicwallet.repositories;

import com.transfert.electronicwallet.domain.entities.AgencyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 08/01/2024
 */
@Repository
public interface AgencyRepository extends JpaRepository<AgencyEntity, String>,
        PagingAndSortingRepository<AgencyEntity, String> {

    Boolean existsByName(String name);

    Optional<AgencyEntity> findByName(String name);
}
