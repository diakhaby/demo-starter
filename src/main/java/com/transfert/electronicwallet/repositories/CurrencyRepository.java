package com.transfert.electronicwallet.repositories;

import com.transfert.electronicwallet.domain.entities.CurrencyEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 11/01/2024
 */
@Repository
public interface CurrencyRepository extends CrudRepository<CurrencyEntity,String>,
        PagingAndSortingRepository<CurrencyEntity,String> {
}
