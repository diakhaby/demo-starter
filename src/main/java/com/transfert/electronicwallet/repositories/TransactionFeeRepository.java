package com.transfert.electronicwallet.repositories;

import com.transfert.electronicwallet.domain.entities.FeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 28/01/2024
 */
@Repository
public interface TransactionFeeRepository extends JpaRepository<FeeEntity, String>,
        PagingAndSortingRepository<FeeEntity, String> {
    Optional<FeeEntity> findByAgencyId(String agencyId);

    Optional<FeeEntity> findByAgencyIdAndMinAmountAndMaxAmount(String agencyId, BigDecimal minAmount, BigDecimal maxAmount);
}
