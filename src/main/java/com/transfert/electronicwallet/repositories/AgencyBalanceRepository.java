package com.transfert.electronicwallet.repositories;

import com.transfert.electronicwallet.domain.entities.AgencyBalanceEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 31/01/2024
 */
@Repository
public interface AgencyBalanceRepository extends CrudRepository<AgencyBalanceEntity, String>,
        PagingAndSortingRepository<AgencyBalanceEntity, String> {

    Optional<AgencyBalanceEntity> findByAgencyId(String agencyId);


    boolean existsByAgencyIdAndCurrencyId(String agencyId, String currencyId);
}
