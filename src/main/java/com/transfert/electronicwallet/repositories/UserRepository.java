package com.transfert.electronicwallet.repositories;

import com.transfert.electronicwallet.domain.entities.AgencyEntity;
import com.transfert.electronicwallet.domain.entities.UserEntity;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String>,
        PagingAndSortingRepository<UserEntity, String> {

    Optional<UserEntity> findByEmail(String email);
    @NotNull
    Optional<UserEntity> findById(@NotNull String id);
    boolean existsByEmail(String email);
}
