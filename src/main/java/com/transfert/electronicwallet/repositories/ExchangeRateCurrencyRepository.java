package com.transfert.electronicwallet.repositories;

import com.transfert.electronicwallet.domain.entities.CurrencyEntity;
import com.transfert.electronicwallet.domain.entities.ExchangeRateCurrencyEntity;
import com.transfert.electronicwallet.services.impl.ExchangeRateCurrencyImpl;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 13/01/2024
 */
@Repository
public interface ExchangeRateCurrencyRepository extends CrudRepository<ExchangeRateCurrencyEntity, String>,
        PagingAndSortingRepository<ExchangeRateCurrencyEntity, String> {
    Optional<ExchangeRateCurrencyEntity> findByCurrencyFromAndCurrencyTo(CurrencyEntity fromCurrency, CurrencyEntity toCurrency);


}
