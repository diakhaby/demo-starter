package com.transfert.electronicwallet.repositories;

import com.transfert.electronicwallet.domain.entities.AgencyBenefitsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 20/02/2024
 */
@Repository
public interface AgencyBenefitsRepository extends JpaRepository<AgencyBenefitsEntity, String>,
        PagingAndSortingRepository<AgencyBenefitsEntity, String> {
    Optional<AgencyBenefitsEntity> findByAgencyIdAndCurrencyId(String id, String id1);

    Optional<AgencyBenefitsEntity> findByAgencyId(String agencyId);
}
