package com.transfert.electronicwallet.dto;

import com.transfert.electronicwallet.domain.entities.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SignUpRequest {
    String firstName;
    String lastName;
    String role;
    String email;
    String password;
    String address;
    String phoneNumber;
    UserEntity dependOnUserEntity;

}
