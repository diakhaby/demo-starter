package com.transfert.electronicwallet.dto;

import com.transfert.electronicwallet.domain.entities.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 05/01/2024
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdateRequest {
    String id;
    String firstName;
    String lastName;
    String role;
    String email;
    String address;
    String phoneNumber;
    UserEntity dependOnUserEntity;

}
