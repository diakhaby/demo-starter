package com.transfert.electronicwallet.controllers;

import com.transfert.electronicwallet.domain.dto.TransactionFeeDto;
import com.transfert.electronicwallet.domain.entities.FeeEntity;
import com.transfert.electronicwallet.mappers.TransactionFeeMapper;
import com.transfert.electronicwallet.services.TransactionFeeService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.BadRequestException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 28/01/2024
 */
@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/api/v1/transactionFee")
public class TransactionFeeController {

    private final TransactionFeeService transactionFeeService;

    private final TransactionFeeMapper transactionFeeMapper;

    @GetMapping
    public Page<TransactionFeeDto> findAll(Pageable pageable) {

        return transactionFeeService.findAll(pageable)
                .map(transactionFeeMapper::toTransactionFeeDto);
    }

    @GetMapping("/{id}")
    public TransactionFeeDto findById(@PathVariable("id") String id) {

        return transactionFeeMapper.toTransactionFeeDto(transactionFeeService.findById(id));
    }

    @PostMapping
    public ResponseEntity<String> save(@RequestBody TransactionFeeDto transactionFeeDto) throws BadRequestException {

        FeeEntity feeEntity = transactionFeeMapper.toTransactionFeeEntity(transactionFeeDto);

        boolean savedTransactionFee = transactionFeeService.save(feeEntity);
        if (savedTransactionFee)
            return new ResponseEntity<>("Transaction fee created successfully", HttpStatus.CREATED);
        else
            return new ResponseEntity<>("Transaction fee not created ",HttpStatus.BAD_REQUEST);
    }
    @PutMapping("/{id}")
    public ResponseEntity<FeeEntity> update(@PathVariable("id") String id, @RequestBody TransactionFeeDto transactionFeeDto) {

        var transactionFeeEntity = transactionFeeMapper.toTransactionFeeEntity(transactionFeeDto);
        transactionFeeService.update(id,transactionFeeEntity);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

