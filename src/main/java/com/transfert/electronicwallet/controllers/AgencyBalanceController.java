package com.transfert.electronicwallet.controllers;

import com.transfert.electronicwallet.domain.dto.AgencyBalanceDto;
import com.transfert.electronicwallet.domain.entities.AgencyBalanceEntity;
import com.transfert.electronicwallet.domain.entities.UserEntity;
import com.transfert.electronicwallet.mappers.AgencyBalanceMapper;
import com.transfert.electronicwallet.services.AgencyBalanceService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 31/01/2024
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/agency-balance")
@Slf4j
public class AgencyBalanceController {

    private final AgencyBalanceService agencyBalanceService;

    private final AgencyBalanceMapper agencyBalanceMapper;

    @GetMapping
    public ResponseEntity<Page<AgencyBalanceEntity>> finAllBalance(@RequestParam(value = "page",defaultValue = "0") int page,
                                                                 @RequestParam(value = "size", defaultValue = "10") int size) {

        return ResponseEntity.ok(agencyBalanceService.findAll(PageRequest.of(page, size)));
    }

    @PostMapping
    public ResponseEntity<String> save(@Valid @RequestBody AgencyBalanceDto agencyBalanceDto) {

            AgencyBalanceEntity agencyBalanceEntity = agencyBalanceMapper.toAgencyBalanceEntity(agencyBalanceDto);

            boolean agencyBalanceSaved = agencyBalanceService.save(agencyBalanceEntity);
        if(agencyBalanceSaved) {
            return new ResponseEntity<>("Balance saved successfully", HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>("Balance not saved - there is already a balance for this agency and currency", HttpStatus.BAD_REQUEST);
        }
    }

   @PutMapping("/{id}")
   public ResponseEntity<AgencyBalanceEntity> update(@PathVariable String id, @Valid @RequestBody AgencyBalanceDto agencyBalanceDto) {
        log.info("the id is : " + id);
        AgencyBalanceEntity agencyBalanceEntity = agencyBalanceMapper.toAgencyBalanceEntity(agencyBalanceDto);
        agencyBalanceService.update(id, agencyBalanceEntity);
        return new ResponseEntity<>(HttpStatus.OK);
   }


}
