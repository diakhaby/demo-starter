package com.transfert.electronicwallet.controllers;

import com.transfert.electronicwallet.domain.dto.UserDto;
import com.transfert.electronicwallet.domain.entities.UserEntity;
import com.transfert.electronicwallet.mappers.Mapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.transfert.electronicwallet.dto.JwtAuthenticationResponse;
import com.transfert.electronicwallet.dto.SignInRequest;
import com.transfert.electronicwallet.dto.SignUpRequest;
import com.transfert.electronicwallet.services.AuthenticationService;

import lombok.RequiredArgsConstructor;


@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@Slf4j
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    private Mapper<UserEntity, UserDto> mapper;

    @PostMapping("/signup")
    public ResponseEntity<JwtAuthenticationResponse> signup(@RequestBody SignUpRequest request) {

        return new ResponseEntity<>(authenticationService.signup(request), HttpStatus.CREATED);
    }



    @PostMapping("/signin")
    public JwtAuthenticationResponse signin(@RequestBody SignInRequest request) {

        return authenticationService.signin(request);
    }
}
