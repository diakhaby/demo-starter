package com.transfert.electronicwallet.controllers;

import com.transfert.electronicwallet.domain.dto.AgencyDto;
import com.transfert.electronicwallet.domain.entities.AgencyEntity;
import com.transfert.electronicwallet.mappers.AgencyMapper;
import com.transfert.electronicwallet.mappers.Mapper;
import com.transfert.electronicwallet.services.AgencyService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.BadRequestException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 08/01/2024
 */

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/agency")
@Slf4j
public class AgencyController {

    private final AgencyService agencyService;

    private final Mapper<AgencyEntity, AgencyDto> mapper;

    private final AgencyMapper agencyMapper;



    @GetMapping
    public Page<AgencyDto> listAgencies(Pageable pageable) {
        Page<AgencyEntity> agencyEntities = agencyService.findAllAgencies(pageable);
        return agencyEntities.map(agencyMapper::toDto);

    }
    @PostMapping
    public ResponseEntity<String> save(@Valid @RequestBody AgencyDto agencyDto) throws BadRequestException {
       AgencyEntity agencyEntity = agencyMapper.toEntity(agencyDto);
       boolean savedAgency = agencyService.save(agencyEntity);
       if(savedAgency)
           return new ResponseEntity<>("Agency saved successfully", HttpStatus.CREATED);
       else
           return new ResponseEntity<>("Agency already exists", HttpStatus.BAD_REQUEST);
    }

    @PutMapping( "/{id}")
    public ResponseEntity<String> save(@Valid @PathVariable String id, @RequestBody AgencyDto agencyDto) throws BadRequestException {
       AgencyEntity agencyEntity = agencyMapper.toEntity(agencyDto);
       boolean updatedAgency = agencyService.update(id,agencyEntity);
       if(updatedAgency)
           return new ResponseEntity<>("Agency updated successfully", HttpStatus.OK);
       else
           return new ResponseEntity<>("Agency not found", HttpStatus.NOT_FOUND);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteAgency(@PathVariable String id) {
        boolean deletedAgency = agencyService.deleteById(id);
        if(deletedAgency)
            return new ResponseEntity<>("Agency deleted successfully", HttpStatus.OK);
        else
            return new ResponseEntity<>("Agency not found", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleMethodNoValidException(MethodArgumentNotValidException exp
    ) {
        var errors = new HashMap<String, String>();

        exp.getBindingResult().getAllErrors()
                .forEach(error -> {
                    var filedName  = ((FieldError) error).getField();
                    var errorMessage =  error.getDefaultMessage();

                    errors.put(filedName,errorMessage);
                });
        return new ResponseEntity<>(errors,HttpStatus.BAD_REQUEST);
    }
}
