package com.transfert.electronicwallet.controllers;


import com.transfert.electronicwallet.domain.dto.ExchangeRateCurrencyDto;
import com.transfert.electronicwallet.domain.entities.CurrencyEntity;
import com.transfert.electronicwallet.domain.entities.ExchangeRateCurrencyEntity;
import com.transfert.electronicwallet.mappers.ExchangeRateCurrencyMapper;
import com.transfert.electronicwallet.mappers.Mapper;

import com.transfert.electronicwallet.services.CurrencyService;
import com.transfert.electronicwallet.services.ExchangeRateCurrencyService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.method.MethodValidationException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Optional;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 14/01/2024
 */

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/exchangeRateCurrency")
@Slf4j
public class ExchangeRateCurrencyController {

    private final ExchangeRateCurrencyService exchangeRateCurrencyService;

    private final ExchangeRateCurrencyMapper exchangeRateCurrencyMapper;

    private final CurrencyService currencyService;

    private final Mapper<ExchangeRateCurrencyEntity, ExchangeRateCurrencyDto> mapper;


    @PostMapping
    public ResponseEntity<String> save(@Valid  @RequestBody ExchangeRateCurrencyDto exchangeRateCurrencyDto) throws NoSuchAlgorithmException {

        ExchangeRateCurrencyEntity  exchangeRateCurrencyEntity = exchangeRateCurrencyMapper.toExchangeRateCurrencyEntity(exchangeRateCurrencyDto);
        boolean saveExchangeRateCurrency = exchangeRateCurrencyService.save(exchangeRateCurrencyEntity);
        if (saveExchangeRateCurrency)
            return new ResponseEntity<>("ExchangeRateCurrency saved successfully",HttpStatus.CREATED);
        else
            return new ResponseEntity<>("ExchangeRateCurrency already exists",HttpStatus.BAD_REQUEST);

    }



    @GetMapping
    public Page<ExchangeRateCurrencyDto> listExchangeRateCurrency(Pageable pageable) {
        return exchangeRateCurrencyService.findAll(pageable)
                .map(exchangeRateCurrencyMapper::toEntity);
    }




    @GetMapping(path = "/{id}")
    public ResponseEntity<ExchangeRateCurrencyDto> getExchangeRateCurrencyById(@NotNull @PathVariable("id") String id) {
        Optional<ExchangeRateCurrencyEntity> exchangeRateCurrencyEntity = exchangeRateCurrencyService.findById(id);
        return exchangeRateCurrencyEntity.map(exchangeRateCurrency -> {
            ExchangeRateCurrencyDto exchangeRateCurrencyDto = mapper.mapTo(exchangeRateCurrency);
            return new ResponseEntity<>(exchangeRateCurrencyDto, HttpStatus.OK);
        }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<String> updateExchangeRateCurrency(@PathVariable String id, @RequestBody ExchangeRateCurrencyDto exchangeRateCurrencyDto) {

        var ExchangeRateCurrencyEntity = exchangeRateCurrencyMapper.toExchangeRateCurrencyEntity(exchangeRateCurrencyDto);
        boolean updated = exchangeRateCurrencyService.update(id, ExchangeRateCurrencyEntity);

        if(updated)
            return new ResponseEntity<>("ExchangeRateCurrency updated successfully",HttpStatus.OK);
        else
            return new ResponseEntity<>("ExchangeRateCurrency not found",HttpStatus.NOT_FOUND);
    }

    @GetMapping(path = "/{currencyFrom}/{currencyTo}")
    public Optional<ExchangeRateCurrencyDto> getExchangeRateCurrency(@PathVariable("currencyFrom") String currencyFrom,
                                                                     @PathVariable("currencyTo") String currencyTo) {
        CurrencyEntity currencyEntityFrom = currencyService.findById(currencyFrom).orElse(null);
        CurrencyEntity currencyEntityTo = currencyService.findById(currencyTo).orElse(null);

        Optional<ExchangeRateCurrencyEntity> exchangeRateCurrencyEntity = exchangeRateCurrencyService.findRate(currencyEntityFrom, currencyEntityTo);

        return exchangeRateCurrencyEntity.map(exchangeRateCurrencyMapper::toEntity);
    }

    @ExceptionHandler(MethodValidationException.class)
    public ResponseEntity<?> handleMethodNoValidException(MethodArgumentNotValidException exp
                                                          ) {
        var errors = new HashMap<String, String>();

        exp.getBindingResult().getAllErrors()
                .forEach(error -> {
                    var filedName  = ((FieldError) error).getField();
                    var errorMessage =  error.getDefaultMessage();

                    errors.put(filedName,errorMessage);
                });
        return new ResponseEntity<>(errors,HttpStatus.BAD_REQUEST);
    }
}
