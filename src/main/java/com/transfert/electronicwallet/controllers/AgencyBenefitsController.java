package com.transfert.electronicwallet.controllers;

import com.transfert.electronicwallet.domain.dto.AgencyBenefitsDto;
import com.transfert.electronicwallet.domain.entities.AgencyBenefitsEntity;
import com.transfert.electronicwallet.mappers.AgencyBenefitsMapper;
import com.transfert.electronicwallet.services.AgencyBenefitsService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 20/02/2024
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/agencyBenefits")
@Slf4j
public class AgencyBenefitsController {

    private final AgencyBenefitsService agencyBenefitsService;

    private final AgencyBenefitsMapper agencyBenefitsMapper;

    @GetMapping
    public Page<AgencyBenefitsDto> allBenefits(Pageable pageable) {

        log.info("Fetching all agency benefits");

        Page<AgencyBenefitsEntity> benefits = agencyBenefitsService.findAll(pageable);

        return benefits.map(agencyBenefitsMapper::toAgencyBenefitsDto);
    }

    @PostMapping
    public ResponseEntity<String> save(@Valid @RequestBody AgencyBenefitsDto agencyBenefitsDto) {

        AgencyBenefitsEntity agencyBenefitsEntity = agencyBenefitsMapper.toAgencyBenefitsEntity(agencyBenefitsDto);
        boolean savedBenefit = agencyBenefitsService.save(agencyBenefitsEntity);

        if (savedBenefit)
            return new ResponseEntity<>("Benefit saved successfully", HttpStatus.CREATED);
        else
            return new ResponseEntity<>("Benefit already exists", HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> save(@Valid @PathVariable String id, @RequestBody AgencyBenefitsDto agencyBenefitsDto) {

        AgencyBenefitsEntity agencyBenefitsEntity = agencyBenefitsMapper.toAgencyBenefitsEntity(agencyBenefitsDto);
        boolean savedBenefit = agencyBenefitsService.update(id, agencyBenefitsEntity);

        if (savedBenefit)
            return new ResponseEntity<>("Benefit updated successfully", HttpStatus.CREATED);
        else
            return new ResponseEntity<>("Benefit was not updated ", HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteAgency(@PathVariable String id) {

        boolean deletedBenefit = agencyBenefitsService.delete(id);
        if(deletedBenefit)
            return new ResponseEntity<>("Benefit deleted successfully", HttpStatus.OK);
        else
            return new ResponseEntity<>("Benefit not found", HttpStatus.NOT_FOUND);
    }
}
