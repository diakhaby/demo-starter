package com.transfert.electronicwallet.controllers;

import com.transfert.electronicwallet.domain.dto.CurrencyDto;
import com.transfert.electronicwallet.domain.entities.CurrencyEntity;
import com.transfert.electronicwallet.mappers.CurrencyMapper;
import com.transfert.electronicwallet.mappers.Mapper;
import com.transfert.electronicwallet.services.CurrencyService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 11/01/2024
 */
@RestController
@RequestMapping("/api/v1/currency")
@RequiredArgsConstructor
@Slf4j
public class CurrencyController {

    private final CurrencyService currencyService;

    private final CurrencyMapper currencyMapper;

    @GetMapping
    public Page<CurrencyDto> currenciesList(Pageable pageable) {
         return currencyService.findAll(pageable)
                 .map(currencyMapper::toDto);

    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<CurrencyDto> currencyById(@PathVariable("id") String id) {
        Optional<CurrencyEntity> currencyEntity = currencyService.findById(id);
        return currencyEntity.map(entity -> new ResponseEntity<>(currencyMapper.toDto(entity), HttpStatus.OK)).orElse(null);
    }

    @PostMapping
    public ResponseEntity<String> save(@Valid @RequestBody CurrencyDto currencyDto) {
        CurrencyEntity currencyEntity = currencyMapper.toEntity(currencyDto);
        CurrencyEntity currencyEntitySaved = currencyService.save(currencyEntity);
        log.info("currencyEntitySaved : {}", currencyEntitySaved);
        return new ResponseEntity<>("Currency saved successfully",HttpStatus.CREATED);

    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<String> updateCurrency(@PathVariable String id, @RequestBody CurrencyDto currencyDto) {
        CurrencyEntity currencyEntity = currencyMapper.toEntity(currencyDto);
         currencyService.update(id, currencyEntity);

        return new ResponseEntity<>("Currency updated successfully",HttpStatus.OK);
    }


    @DeleteMapping(path = "/{id}")
    public ResponseEntity<String> deleteCurrency(@PathVariable String id) {
        boolean deletedCurrency = currencyService.deleteById(id);
        if(deletedCurrency)
            return new ResponseEntity<>("Currency deleted successfully",HttpStatus.OK);
        else
            return new ResponseEntity<>("Currency not found",HttpStatus.NOT_FOUND);
    }
}
