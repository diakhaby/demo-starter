package com.transfert.electronicwallet.controllers;

import com.transfert.electronicwallet.domain.entities.UserEntity;
import com.transfert.electronicwallet.repositories.UserRepository;
import com.transfert.electronicwallet.services.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.transfert.electronicwallet.constant.Constant.PHOTO_DIRECTORY;
import static org.springframework.http.MediaType.IMAGE_JPEG_VALUE;
import static org.springframework.http.MediaType.IMAGE_PNG_VALUE;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 28/12/2023
 */
@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
@Slf4j
@CrossOrigin(origins = "http://localhost:3000")
public class UserController {

    private final UserService userService;
    private final UserRepository userRepository;
    @GetMapping("/admin")
    //@SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<Page<UserEntity>> getAllUsers(
            @RequestParam(value = "page",defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size
            ) {
        log.info("getAllUsers");
        return ResponseEntity.ok(userService.getAllUsers(page, size));
    }

    @GetMapping("/{id}")
    @SecurityRequirement(name = "Bearer Authentication")
    public ResponseEntity<UserEntity> getUser(@PathVariable(value = "id") String id) {
        return ResponseEntity.ok().body(userService.getUserById(id));
    }

    @PutMapping("/update/{id}")
    UserEntity updateUser(@RequestBody UserEntity newUserEntity, @PathVariable String id) {
        return userRepository.findById(id).map(user -> {
            user.setFirstName(newUserEntity.getFirstName());
            user.setLastName(newUserEntity.getLastName());
            user.setEmail(newUserEntity.getEmail());
            //user.setRole(newUser.getRole());
            user.setAddress(newUserEntity.getAddress());
            user.setPhoneNumber(newUserEntity.getPhoneNumber());
            return userRepository.save(user);
        }).orElseThrow(() -> new RuntimeException("User not found"));
    }

   @PutMapping("/photo")
   public ResponseEntity<String> uploadPhoto(@RequestParam("id") String id, @RequestParam("file") MultipartFile file) {

        return ResponseEntity.ok().body(userService.uploadPhoto(id, file));
   }

   @GetMapping(path = "/image/{fileName}", produces = { IMAGE_PNG_VALUE, IMAGE_JPEG_VALUE })
    public byte[] getPhoto(@PathVariable("fileName") String fileName) throws IOException {
        return Files.readAllBytes(Paths.get(PHOTO_DIRECTORY, fileName));
    }


    @GetMapping("/userLogged")
    public ResponseEntity<UserDetails> getUserlogged() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserEntity userDetails = (UserEntity) authentication.getPrincipal();
        log.info(userDetails.getEmail());
        return ResponseEntity.ok(userDetails);
    }

}
