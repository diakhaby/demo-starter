package com.transfert.electronicwallet.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CurrentTimestamp;
import org.hibernate.annotations.UuidGenerator;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 10/01/2024
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "exchange_rate", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"rate","currency_from", "currency_to"})
})
public class ExchangeRateCurrencyEntity {

    @Id
    @UuidGenerator
    @Column(updatable = false, nullable = false, unique = true, length = 36)
    private String id;

    @Column(name = "rate",scale = 5,precision = 10)
    private BigDecimal rate;

    @CurrentTimestamp
    private LocalDateTime createdAt;

    @CurrentTimestamp
    private LocalDateTime updatedAt;

    @ManyToOne
    @JoinColumn(name = "updated_by",foreignKey = @ForeignKey(name = "fk_updated_by"),nullable = true)
    @JsonManagedReference
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name = "created_by",foreignKey = @ForeignKey(name = "fk_created_by_exchange_rate"),nullable = true)
    @JsonManagedReference
    private UserEntity createdby;

    boolean isActive = true;

    @ManyToOne
    @JoinColumn(name = "currency_from",foreignKey = @ForeignKey(name = "fk_currency_from"))
    @JsonManagedReference
    private CurrencyEntity currencyFrom;

    @ManyToOne
    @JoinColumn(name = "currency_to",foreignKey = @ForeignKey(name = "fk_currency_to"))
    @JsonManagedReference
    private CurrencyEntity currencyTo;

}
