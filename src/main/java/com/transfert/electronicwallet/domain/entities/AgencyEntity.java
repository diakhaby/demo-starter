package com.transfert.electronicwallet.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CurrentTimestamp;
import org.hibernate.annotations.UuidGenerator;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 08/01/2024
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "agency", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"name"}),

})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AgencyEntity {

    @Id
    @UuidGenerator
    @Column(name = "id",unique = true, updatable = false, length = 36)
    private String id;

    @Column(unique = true,nullable = false)
    private String name;

    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private String phoneNumber;

    @Column(nullable = false)
    private String email;

    @CurrentTimestamp
    LocalDateTime createdAt;

    @CurrentTimestamp
    LocalDateTime updatedAt;

    @ManyToOne
    @JoinColumn(name = "created_by",nullable = true,foreignKey = @ForeignKey(name = "fk_created_by"))
    @JsonBackReference
    private UserEntity user;

}
