package com.transfert.electronicwallet.domain.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CurrentTimestamp;
import org.hibernate.annotations.UuidGenerator;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 20/02/2024
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "agency_benefits")
public class AgencyBenefitsEntity {

    @Id
    @UuidGenerator
    @Column(name = "id",unique = true, updatable = false, length = 36)
    private String id;

    private BigDecimal amount;

    @ManyToOne
    private AgencyEntity agency;

    @ManyToOne
    private CurrencyEntity currency;

    @ManyToOne
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name = "updated_by", referencedColumnName = "id",foreignKey = @ForeignKey(name = "fk_updated_by_agency_benefits"))
    private UserEntity updatedBy;

    @CurrentTimestamp
    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    private boolean isActive = true;
}
