package com.transfert.electronicwallet.domain.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CurrentTimestamp;
import org.hibernate.annotations.UuidGenerator;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 10/01/2024
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "transfer")
public class TransferEntity {

    @Id
    @UuidGenerator
    @Column(name = "id",unique = true, updatable = false, length = 36)
    private String id;

    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_type",nullable = false)
    TransactionType type;

    @Positive
    @Column(name = "amount_sent",nullable = false)
    private BigDecimal amountSent;

    @Positive
    @Column(name = "amount_to_received",nullable = false)
    private BigDecimal amountToReceived;

    @PositiveOrZero
    @Column(name = "rest_to_receive",nullable = false)
    private BigDecimal restToReceive;

    @Positive
    @Column(name = "exchange_rate",nullable = false)
    private double exchangeRate;

    @Positive
    private double feeRate;

    @Positive
    private BigDecimal feeAmount;

    private String code;

    @ManyToOne
    private AgencyEntity agencysender;

    @ManyToOne
    private AgencyEntity agencyreceiver;

    @ManyToOne
    private CurrencyEntity currencyfrom;

    @ManyToOne
    private CurrencyEntity currencyto;

    @ManyToOne
    private UserEntity client;

    @ManyToOne
    private UserEntity beneficiary;

    @ManyToOne
    private UserEntity createdby;

    @CurrentTimestamp
    private LocalDateTime createdAt;

    @ManyToOne
    private UserEntity updatedby;

    private LocalDateTime updatedAt;

    @ManyToOne
    private UserEntity paidby;

    @Column(name = "paid_at", nullable = true)
    private LocalDateTime paidAt;

    @ManyToOne
    private UserEntity cancelledby;

    @Column(name = "cancelled_at", nullable = true)
    private LocalDateTime cancelledAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private TransactionStatus status;
}
