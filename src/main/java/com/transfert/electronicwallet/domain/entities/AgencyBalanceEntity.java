package com.transfert.electronicwallet.domain.entities;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CurrentTimestamp;
import org.hibernate.annotations.UuidGenerator;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 31/01/2024
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "agency_balances")
public class AgencyBalanceEntity {

    @Id
    @UuidGenerator
    @Column(updatable = false, nullable = false,length = 36)
    private String id;

    @ManyToOne
    @JoinColumn(name = "currency_id",foreignKey = @ForeignKey(name = "fk_balance_currency"),nullable = false)
    private CurrencyEntity currency;

    private BigDecimal amount;

    @ManyToOne
    @JoinColumn(name = "agency_id",foreignKey = @ForeignKey(name = "fk_agency"),nullable = false)
    private AgencyEntity agency;

    @ManyToOne
    @JoinColumn(name = "created_by",foreignKey = @ForeignKey(name = "fk_balance_created_by"),nullable = false)
    private UserEntity user;

    private boolean isActive = true;

    @CurrentTimestamp
    LocalDateTime created;

    @CurrentTimestamp
    LocalDateTime updated;
}
