package com.transfert.electronicwallet.domain.entities;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 20/02/2024
 */
public enum TransactionStatus {
    CREE,
    PAYE,
    PARTIELLEMENT_PAYE,
    ANNULE
}
