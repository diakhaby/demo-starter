package com.transfert.electronicwallet.domain.entities;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CurrentTimestamp;
import org.hibernate.annotations.UuidGenerator;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 11/01/2024
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "transaction_fee")
public class FeeEntity {

    @Id
    @UuidGenerator
    @Column(name = "id",unique = true, updatable = false, length = 36)
    private String id;

    private BigDecimal minAmount;

    private BigDecimal maxAmount;

    private BigDecimal fixedFee;

    private BigDecimal percentFee;

    @ManyToOne
    private AgencyEntity agency;

    @ManyToOne
    private UserEntity user;

    @CurrentTimestamp
    LocalDateTime createdAt;

    @CurrentTimestamp
    LocalDateTime updatedAt;

    @Column(name = "is_active")
    boolean isActive = true;

}
