package com.transfert.electronicwallet.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CurrentTimestamp;
import org.hibernate.annotations.UuidGenerator;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 10/01/2024
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "currency", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"name","iso", "country_code"})
})
public class CurrencyEntity {

   @Id
   @UuidGenerator
   @Column(updatable = false, nullable = false, unique = true, length = 36)
    private String id;

    private String name;

    private String iso;

    private String countryCode;

    @ManyToOne
    @JoinColumn(name = "created_by",foreignKey = @ForeignKey(name = "fk_created_by_currency"),nullable = false)
    @JsonManagedReference
    private UserEntity user;

    @CurrentTimestamp
    private LocalDateTime createdAt;

    @CurrentTimestamp
    private LocalDateTime updatedAt;

    boolean isActive = true;

}
