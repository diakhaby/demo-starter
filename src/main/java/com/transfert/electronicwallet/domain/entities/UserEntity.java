package com.transfert.electronicwallet.domain.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import org.hibernate.annotations.CurrentTimestamp;
import org.hibernate.annotations.UuidGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "users")
public class UserEntity implements UserDetails {

    @Id
    @UuidGenerator
    @Column(name = "id",unique = true,nullable = false, updatable = false, length = 36)
    private String id;

    private String firstName;

    private String lastName;

    @Column(unique = true)
    private String email;

    private String password;

    private String address;

    private String phoneNumber;

    private String photoUrl;

    boolean isActive = true;

    @ManyToOne
    @JoinColumn(name = "parent_id",nullable = true)
    @JsonManagedReference
    @JsonIgnore
    private UserEntity dependOnUserEntity;

    @CurrentTimestamp
    LocalDateTime createdAt;

    @CurrentTimestamp
    LocalDateTime updatedAt;

    private LocalDateTime lastLogin;

       @Enumerated(EnumType.STRING)
       @Column(name = "role")
       Role role;
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


    public UserEntity(String id) {
        this.id = id;
    }

    // Méthode de fabrication (factory method) prenant une chaîne de caractères en argument
    public static UserEntity fromString(String id) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(id);
        return userEntity;
    }

}
