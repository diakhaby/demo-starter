package com.transfert.electronicwallet.domain.entities;

public enum Role {
    ROLE_ADMIN,
    ROLE_USER
}
