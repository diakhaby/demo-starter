package com.transfert.electronicwallet.domain.entities;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 20/02/2024
 */
public enum TransactionType {

    CASH,
    ORANGE_MONEY,
    MOOV_MONEY,
    CREDIT_RECHARGE,
    CREDIT_INTERNET

}
