package com.transfert.electronicwallet.domain.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 28/01/2024
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionFeeDto {
        BigDecimal minAmount;
        BigDecimal maxAmount;
        BigDecimal fixedFee;
        BigDecimal percentFee;
        String agency;
}
