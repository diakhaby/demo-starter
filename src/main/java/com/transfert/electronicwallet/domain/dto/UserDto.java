package com.transfert.electronicwallet.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 09/01/2024
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {
    String id;
    String firstName;
    String lastName;
    String role;
    String email;
    String password;
    String address;
    String phoneNumber;
    //UserDto dependOnUser;
}
