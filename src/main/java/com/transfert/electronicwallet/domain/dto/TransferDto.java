package com.transfert.electronicwallet.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 20/02/2024
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TransferDto {

    String type;

    BigDecimal amountSent;

    BigDecimal amountToReceived;

    BigDecimal restToReceive;

    double exchangeRate;

    double feeRate;

    BigDecimal feeAmount;

    String code;

    String agencySender;

    String agencyReceiver;

    String currencyFrom;

    String currencyTo;

    String client;

    String beneficiary;

    String status;
}
