package com.transfert.electronicwallet.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.math.BigDecimal;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 13/01/2024
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExchangeRateCurrencyDto {
        BigDecimal rate;
        String currencyFrom;
        String currencyTo;
}
