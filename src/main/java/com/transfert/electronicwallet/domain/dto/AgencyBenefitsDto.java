package com.transfert.electronicwallet.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 20/02/2024
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AgencyBenefitsDto {

        String currency;
        String agency;
        BigDecimal amount;
}
