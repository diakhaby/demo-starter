package com.transfert.electronicwallet.domain.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 31/01/2024
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AgencyBalanceDto {
        String currency;
        String agency;
        BigDecimal amount;
}
