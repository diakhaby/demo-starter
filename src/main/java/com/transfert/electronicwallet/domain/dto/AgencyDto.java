package com.transfert.electronicwallet.domain.dto;

import com.transfert.electronicwallet.domain.entities.UserEntity;
import lombok.*;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 08/01/2024
 */


@AllArgsConstructor
@NoArgsConstructor
@Data
public class AgencyDto {
    private String name;
    private String address;
    private String phoneNumber;
    private String email;
    private UserEntity user;
}
