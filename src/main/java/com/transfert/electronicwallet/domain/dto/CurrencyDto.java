package com.transfert.electronicwallet.domain.dto;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 11/01/2024
 */

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CurrencyDto{

    String name;
    String iso;
    String countryCode;
}
