package com.transfert.electronicwallet.constant;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 28/12/2023
 */
public class Constant {
    public static final String PHOTO_DIRECTORY = System.getProperty("user.home") + "/Downloads/uploads";

    public static final String X_REQUESTED_WITH = "X-Requested-With";
}
