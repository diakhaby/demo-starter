package com.transfert.electronicwallet.services.impl;

import com.transfert.electronicwallet.domain.entities.CurrencyEntity;
import com.transfert.electronicwallet.repositories.CurrencyRepository;
import com.transfert.electronicwallet.services.CurrencyService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 11/01/2024
 */
@Service
@AllArgsConstructor
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepository currencyRepository;
    @Override
    public Page<CurrencyEntity> findAll(Pageable pageable) {
        return currencyRepository.findAll(pageable);
    }

    @Override
    public CurrencyEntity save(CurrencyEntity currencyEntity) {
        return currencyRepository.save(currencyEntity);
    }

    @Override
    public CurrencyEntity update(String id, CurrencyEntity currencyEntity) {

        currencyEntity.setId(id);

        return currencyRepository.save(currencyEntity);
    }

    @Override
    public Optional<CurrencyEntity> findById(String id) {
        return currencyRepository.findById(id);
    }

    @Override
    public boolean existsById(String id) {
        return currencyRepository.existsById(id);
    }

    @Override
    public boolean deleteById(String id) {
        Optional<CurrencyEntity> currencyEntity = currencyRepository.findById(id);

        if(currencyEntity.isPresent()){
            currencyRepository.deleteById(id);
            return true;
        }else {

            return false;
        }

    }
}
