package com.transfert.electronicwallet.services.impl;

import com.transfert.electronicwallet.domain.entities.AgencyEntity;
import com.transfert.electronicwallet.domain.entities.TransferEntity;
import com.transfert.electronicwallet.repositories.TransferRepository;
import com.transfert.electronicwallet.services.TransferService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 20/02/2024
 */
@Service
@AllArgsConstructor
public class TransferServiceImpl implements TransferService {

    private final TransferRepository transferRepository;
    @Override
    public Page<TransferEntity> findAll(Pageable pageable) {
        return transferRepository.findAll(pageable);
    }

    @Override
    public Page<TransferEntity> findByAgencySender(AgencyEntity agencySender, Pageable pageable) {
        return transferRepository.findByAgencysender(agencySender, pageable);
    }

    @Override
    public Page<TransferEntity> findByAgencyReceiver(AgencyEntity agencyReceiver, Pageable pageable) {
        return transferRepository.findByAgencyreceiver(agencyReceiver, pageable);
    }

    @Override
    public Page<TransferEntity> findByAgencySenderAndAgencyReceiver(String agencySender, String agencyReceiver, Pageable pageable) {
        return null;
    }

    @Override
    public Page<TransferEntity> findByClient(String client, Pageable pageable) {
        return null;
    }

    @Override
    public Page<TransferEntity> findByClientAndStatus(String client, String status, Pageable pageable) {
        return null;
    }

    @Override
    public Page<TransferEntity> findByStatus(String status, Pageable pageable) {
        return null;
    }

    @Override
    public Page<TransferEntity> findByClientAndType(String client, String type, Pageable pageable) {
        return null;
    }

    @Override
    public Page<TransferEntity> findByType(String type, Pageable pageable) {
        return null;
    }

    @Override
    public boolean existsById(String id) {
        return false;
    }

    @Override
    public boolean existsByCode(String code) {
        return false;
    }

    @Override
    public boolean save(TransferEntity transfer) {
        return false;
    }

    @Override
    public TransferEntity findById(String id) {
        return null;
    }

    @Override
    public TransferEntity findByCode(String code) {
        return null;
    }

    @Override
    public boolean update(String id, TransferEntity transfer) {
        return false;
    }

    @Override
    public boolean pay(TransferEntity transfer) {
        return false;
    }

    @Override
    public boolean cancel(TransferEntity transfer) {
        return false;
    }

    @Override
    public boolean deleteById(String id) {
        return false;
    }
}
