package com.transfert.electronicwallet.services.impl;

import com.transfert.electronicwallet.domain.entities.AgencyEntity;
import com.transfert.electronicwallet.repositories.AgencyRepository;
import com.transfert.electronicwallet.services.AgencyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 08/01/2024
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class AgencyServiceImpl implements AgencyService {

    private final AgencyRepository agencyRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Override
    public boolean save(AgencyEntity agencyEntity) throws BadRequestException {


        Optional<AgencyEntity> optionalAgencyEntity = agencyRepository.findByName(agencyEntity.getName());
        if (optionalAgencyEntity.isPresent())
            return false;
        else
             agencyRepository.save(agencyEntity);
        return true;
    }

   /* @Override
    public AgencyEntity saveAgency(AgencyEntity agencyEntity) {
        Optional<AgencyEntity> optionalAgencyEntity = agencyRepository.findByName(agencyEntity.getName());

        if (optionalAgencyEntity.isEmpty())
            return agencyRepository.save(agencyEntity);
        else
            return null;
    }*/

    @Override
    public Optional<AgencyEntity> findById(String id) {
        return agencyRepository.findById(id);
    }

    @Override
    public Boolean existsById(String id) {
        return agencyRepository.existsById(id);
    }

    @Override
    public List<AgencyEntity> findAll() {
        return StreamSupport
                .stream(
                        agencyRepository.findAll().spliterator(), false
                )
                .collect(Collectors.toList());
    }

    @Override
    public Page<AgencyEntity> findAllAgencies(Pageable pageable) {
        return agencyRepository.findAll(pageable);
    }

    @Override
    public void partialUpdate(String id, AgencyEntity agencyEntity) {

        agencyEntity.setId(id);
        agencyRepository.save(agencyEntity);
       /*return agencyRepository.findById(id).map(agency -> {
            log.info("Updating agency with ID: {}", id);
           Optional.ofNullable(agencyEntity.getName()).ifPresent(agency::setName);
           Optional.ofNullable(agencyEntity.getAddress()).ifPresent(agency::setAddress);
           Optional.ofNullable(agencyEntity.getPhoneNumber()).ifPresent(agency::setPhoneNumber);
           Optional.ofNullable(agencyEntity.getEmail()).ifPresent(agency::setEmail);
           Optional.ofNullable(agencyEntity.getUser()).ifPresent(agency::setUser);
           return agencyRepository.save(agency);
        }).orElseThrow(() -> new RuntimeException("Agency not found"));*/
    }

    @Override
    public boolean update(String id,AgencyEntity agencyEntity) {
        log.info("Updating agency with ID: {}", id);
        Optional<AgencyEntity> agencyEntityOptional = agencyRepository.findById(id);
        if (agencyEntityOptional.isPresent()) {
            AgencyEntity agency = agencyEntityOptional.get();
            agency.setId(id);
            agency.setName(agencyEntity.getName());
            agency.setEmail(agencyEntity.getEmail());
            agency.setAddress(agencyEntity.getAddress());
            agency.setPhoneNumber(agencyEntity.getPhoneNumber());
            agencyRepository.save(agency);
            return true;
        } else {
            return false;
        }

    }

    @Override
    public boolean deleteById(String id) {
        Optional<AgencyEntity> agencyEntity = agencyRepository.findById(id);
        if (agencyEntity.isPresent()){
            agencyRepository.deleteById(id);
            return true;
        }else{
            return false;
        }
    }
}
