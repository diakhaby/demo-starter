package com.transfert.electronicwallet.services.impl;

import com.transfert.electronicwallet.config.Services;
import com.transfert.electronicwallet.domain.entities.AgencyBenefitsEntity;
import com.transfert.electronicwallet.domain.entities.UserEntity;
import com.transfert.electronicwallet.repositories.AgencyBenefitsRepository;
import com.transfert.electronicwallet.services.AgencyBenefitsService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 20/02/2024
 */
@Service
@AllArgsConstructor
public class AgencyBenefitsServiceImpl implements AgencyBenefitsService {

    private final AgencyBenefitsRepository agencyBenefitsRepository;
    @Override
    public Page<AgencyBenefitsEntity> findAll(Pageable pageable) {
        return agencyBenefitsRepository.findAll(pageable);
    }

    @Override
    public boolean save(AgencyBenefitsEntity agencyBenefitsEntity) {
        // check if exists
        Optional<AgencyBenefitsEntity> agencyBenefitsExist = agencyBenefitsRepository.findByAgencyIdAndCurrencyId(agencyBenefitsEntity.getAgency().getId(), agencyBenefitsEntity.getCurrency().getId());

        if(agencyBenefitsExist.isPresent()) {
            return false;
        }
        agencyBenefitsEntity.setUpdatedAt(LocalDateTime.now());
        agencyBenefitsRepository.save(agencyBenefitsEntity);

        return true;
    }

    @Override
    public boolean existsByAgencyId(String agencyId) {
        AgencyBenefitsEntity agencyBenefitsEntity = this.findByAgencyId(agencyId);

        return agencyBenefitsEntity != null;
    }

    @Override
    public AgencyBenefitsEntity findByAgencyId(String agencyId) {
        return agencyBenefitsRepository.findByAgencyId(agencyId).orElse(null);
    }

    @Override
    public AgencyBenefitsEntity findByAgencyIdAndCurrencyId(String agencyId, String currencyId) {
        return agencyBenefitsRepository.findByAgencyIdAndCurrencyId(agencyId, currencyId).orElse(null);
    }

    @Override
    public AgencyBenefitsEntity findById(String id) {
        return agencyBenefitsRepository.findById(id).orElse(null);
    }

    @Override
    public boolean update(String id, AgencyBenefitsEntity agencyBenefitsEntity) {
        AgencyBenefitsEntity agencyBenefits = this.findById(id);
        UserEntity userLogged = Services.userLogin();
        if (agencyBenefits != null) {
            agencyBenefits.setAmount(agencyBenefitsEntity.getAmount());
            agencyBenefits.setCurrency(agencyBenefitsEntity.getCurrency());
            agencyBenefits.setAgency(agencyBenefitsEntity.getAgency());
            agencyBenefits.setUpdatedBy(userLogged);
            agencyBenefits.setUpdatedAt(LocalDateTime.now());
            agencyBenefitsRepository.save(agencyBenefits);
            return true;
        }
        return false;
    }

    @Override
    public boolean existsById(String id) {
        return agencyBenefitsRepository.existsById(id);
    }

    @Override
    public boolean delete(String id) {
        AgencyBenefitsEntity agencyBenefitsEntity = this.findById(id);
        if (agencyBenefitsEntity != null) {
            agencyBenefitsRepository.delete(agencyBenefitsEntity);
            return true;
        }
        return false;
    }
}
