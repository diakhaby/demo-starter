package com.transfert.electronicwallet.services.impl;

import com.transfert.electronicwallet.domain.entities.FeeEntity;
import com.transfert.electronicwallet.repositories.TransactionFeeRepository;
import com.transfert.electronicwallet.services.TransactionFeeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 28/01/2024
 */
@Service
@AllArgsConstructor
@Slf4j
public class TransactionFeeServiceImpl implements TransactionFeeService {

    private final TransactionFeeRepository transactionFeeRepository;

    @Override
    public boolean save(FeeEntity feeEntity) {
         transactionFeeRepository.save(feeEntity);
         return true;
    }

    @Override
    public boolean update(String id, FeeEntity feeEntity) {
        Optional<FeeEntity> optionalTransactionFeeEntity = transactionFeeRepository.findById(id);

        if(optionalTransactionFeeEntity.isPresent()){


            FeeEntity updatedFeeEntity = optionalTransactionFeeEntity.get();
            updatedFeeEntity.setId(id);
            updatedFeeEntity.setMinAmount(feeEntity.getMinAmount());
            updatedFeeEntity.setMaxAmount(feeEntity.getMaxAmount());
            updatedFeeEntity.setFixedFee(feeEntity.getFixedFee());
            updatedFeeEntity.setPercentFee(feeEntity.getPercentFee());
            updatedFeeEntity.setAgency(feeEntity.getAgency());

            transactionFeeRepository.save(updatedFeeEntity);
            return true;

        }else{
            return false;
        }
    }

    @Override
    public FeeEntity findById(String id) {

        return transactionFeeRepository.findById(id).orElse(null);
    }

    @Override
    public FeeEntity findByAgencyId(String agencyId) {

        return transactionFeeRepository.findByAgencyId(agencyId).orElse(null);
    }

    @Override
    public FeeEntity findByAgencyIdAndMinAmountAndMaxAmount(String agencyId, BigDecimal minAmount, BigDecimal maxAmount) {
        return transactionFeeRepository.findByAgencyIdAndMinAmountAndMaxAmount(agencyId, minAmount, maxAmount).orElse(null);
    }

    @Override
    public Page<FeeEntity> findAll(Pageable pageable) {

        return transactionFeeRepository.findAll(pageable);
    }

    @Override
    public boolean deleteById(String id) {

        transactionFeeRepository.deleteById(id);
        return true;
    }
}
