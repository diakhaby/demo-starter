package com.transfert.electronicwallet.services.impl;

import com.transfert.electronicwallet.config.Services;
import com.transfert.electronicwallet.domain.entities.CurrencyEntity;
import com.transfert.electronicwallet.domain.entities.ExchangeRateCurrencyEntity;
import com.transfert.electronicwallet.domain.entities.UserEntity;
import com.transfert.electronicwallet.repositories.CurrencyRepository;
import com.transfert.electronicwallet.repositories.ExchangeRateCurrencyRepository;
import com.transfert.electronicwallet.services.CurrencyService;
import com.transfert.electronicwallet.services.ExchangeRateCurrencyService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 13/01/2024
 */
@Service
@AllArgsConstructor
@Slf4j
public class ExchangeRateCurrencyImpl implements ExchangeRateCurrencyService {

    private final ExchangeRateCurrencyRepository exchangeRateCurrencyRepository;

    private final CurrencyService currencyService;

    @Override
    public boolean save(ExchangeRateCurrencyEntity exchangeRateCurrency) {


         exchangeRateCurrencyRepository.save(exchangeRateCurrency);
        return true;
        //return exchangeRateCurrencyRepository.save(exchangeRateCurrency);
    }

    @Override
    public Optional<ExchangeRateCurrencyEntity> findById(String id) {
        return exchangeRateCurrencyRepository.findById(id);
    }

    @Override
    public Boolean existsById(String id) {
        return null;
    }

    @Override
    public Page<ExchangeRateCurrencyEntity> findAll(Pageable pageable) {
        return exchangeRateCurrencyRepository.findAll(pageable);
    }

    @Override
    public boolean update(String id, ExchangeRateCurrencyEntity exchangeRateCurrency) {
        exchangeRateCurrency.setId(id);
        // Get User logged in
        UserEntity userLoggedIn = Services.userLogin();
        Optional<ExchangeRateCurrencyEntity> exchangeRateCurrencyEntityOptional = exchangeRateCurrencyRepository.findById(id);

        if(exchangeRateCurrencyEntityOptional.isPresent()){
            ExchangeRateCurrencyEntity exchangeRateCurrencyEntityUpdated = exchangeRateCurrencyEntityOptional.get();
            exchangeRateCurrencyEntityUpdated.setUser(userLoggedIn);
            exchangeRateCurrencyEntityUpdated.setRate(exchangeRateCurrency.getRate());
            exchangeRateCurrencyEntityUpdated.setCurrencyFrom(exchangeRateCurrency.getCurrencyFrom());
            exchangeRateCurrencyEntityUpdated.setCurrencyTo(exchangeRateCurrency.getCurrencyTo());
            exchangeRateCurrencyEntityUpdated.setUpdatedAt(LocalDateTime.now());
            exchangeRateCurrencyRepository.save(exchangeRateCurrencyEntityUpdated);
            return true;
        } else{
            return false;
        }
    }

    @Override
    public Optional<ExchangeRateCurrencyEntity> findRate(CurrencyEntity fromCurrency, CurrencyEntity toCurrency) {
        return exchangeRateCurrencyRepository.findByCurrencyFromAndCurrencyTo(fromCurrency, toCurrency);
    }

}
