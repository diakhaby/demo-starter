package com.transfert.electronicwallet.services.impl;

import com.transfert.electronicwallet.domain.entities.AgencyBalanceEntity;
import com.transfert.electronicwallet.repositories.AgencyBalanceRepository;
import com.transfert.electronicwallet.services.AgencyBalanceService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 31/01/2024
 */
@Service
@AllArgsConstructor
public class AgencyBalanceImpl implements AgencyBalanceService {

    private final AgencyBalanceRepository agencyBalanceRepository;
    @Override
    public Page<AgencyBalanceEntity> findAll(Pageable pageable) {
        return agencyBalanceRepository.findAll(pageable);
    }


    public Page<AgencyBalanceEntity>getAllBalances(int page, int size){
        return agencyBalanceRepository.findAll(PageRequest.of(page, size, Sort.by("createdAt").descending()));
    }
    @Override
    public boolean save(AgencyBalanceEntity balanceEntity) {
        var existRecord = agencyBalanceRepository.existsByAgencyIdAndCurrencyId(balanceEntity.getAgency().getId(), balanceEntity.getCurrency().getId());
        if(existRecord) {
            return false;
        }else {
            agencyBalanceRepository.save(balanceEntity);
            return true;
        }

    }

    @Override
    public AgencyBalanceEntity update(String id, AgencyBalanceEntity balanceEntity) {
        balanceEntity.setId(id);
        return agencyBalanceRepository.save(balanceEntity);
    }

    @Override
    public AgencyBalanceEntity findById(String id) {
        return agencyBalanceRepository.findById(id).orElse(null);
    }

    @Override
    public AgencyBalanceEntity findByAgencyId(String agencyId) {
        return agencyBalanceRepository.findByAgencyId(agencyId).orElse(null);
    }

    @Override
    public boolean existsById(String id) {
        return agencyBalanceRepository.existsById(id);
    }

    public boolean existsByAgencyIdAndCurrencyId(String agencyId, String currencyId){
        return agencyBalanceRepository.existsByAgencyIdAndCurrencyId(agencyId, currencyId);

    }

    @Override
    public void deleteById(String id) {
        agencyBalanceRepository.deleteById(id);
    }
}
