package com.transfert.electronicwallet.services;

import com.transfert.electronicwallet.domain.entities.AgencyBalanceEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 31/01/2024
 */

@Service
public interface AgencyBalanceService {

    Page<AgencyBalanceEntity> findAll(Pageable pageable);

    Page<AgencyBalanceEntity> getAllBalances(int page, int size);

    boolean save(AgencyBalanceEntity balanceEntity);

    AgencyBalanceEntity update(String id, AgencyBalanceEntity balanceEntity);

    AgencyBalanceEntity findById(String id);

    AgencyBalanceEntity findByAgencyId(String agencyId);

    boolean existsById(String id);

    void deleteById(String id);
}
