package com.transfert.electronicwallet.services;

import com.transfert.electronicwallet.domain.entities.FeeEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 28/01/2024
 */
@Service
public interface TransactionFeeService {

    boolean save(FeeEntity feeEntity);

    boolean update(String id, FeeEntity feeEntity);

    FeeEntity findById(String id);

    FeeEntity findByAgencyId(String agencyId);

    FeeEntity findByAgencyIdAndMinAmountAndMaxAmount(String agencyId, BigDecimal minAmount, BigDecimal maxAmount);

    Page<FeeEntity> findAll(Pageable pageable);

    boolean deleteById(String id);
}
