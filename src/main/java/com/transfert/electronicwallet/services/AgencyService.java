package com.transfert.electronicwallet.services;

import com.transfert.electronicwallet.domain.entities.AgencyEntity;
import lombok.AllArgsConstructor;
import org.apache.coyote.BadRequestException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 08/01/2024
 */

@Service
public interface AgencyService {

    boolean save(AgencyEntity agencyEntity) throws BadRequestException;

    //AgencyEntity saveAgency(AgencyEntity agencyEntity);

    Optional<AgencyEntity> findById(String id);

    Boolean existsById(String id);

    List<AgencyEntity> findAll();


    Page<AgencyEntity> findAllAgencies(Pageable pageable);

    void partialUpdate(String id, AgencyEntity agencyEntity);

    boolean update(String id,AgencyEntity agencyEntity);

    boolean deleteById(String id);
}
