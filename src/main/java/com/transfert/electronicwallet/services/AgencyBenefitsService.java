package com.transfert.electronicwallet.services;

import com.transfert.electronicwallet.domain.entities.AgencyBenefitsEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 20/02/2024
 */
@Service
public interface AgencyBenefitsService {

    Page<AgencyBenefitsEntity> findAll(Pageable pageable);

    boolean save(AgencyBenefitsEntity agencyBenefitsEntity);

    boolean existsByAgencyId(String agencyId);

    AgencyBenefitsEntity findByAgencyId(String agencyId);

    AgencyBenefitsEntity findByAgencyIdAndCurrencyId(String agencyId, String currencyId);

    AgencyBenefitsEntity findById(String id);

    boolean update(String id, AgencyBenefitsEntity agencyBenefitsEntity);

    boolean existsById(String id);

    boolean delete(String id);
}
