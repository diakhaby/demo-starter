package com.transfert.electronicwallet.services;

import com.transfert.electronicwallet.domain.entities.UserEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.transfert.electronicwallet.dto.JwtAuthenticationResponse;
import com.transfert.electronicwallet.dto.SignInRequest;
import com.transfert.electronicwallet.dto.SignUpRequest;

import com.transfert.electronicwallet.domain.entities.Role;
import com.transfert.electronicwallet.repositories.UserRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthenticationService {

    private final UserRepository userRepository;
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public JwtAuthenticationResponse signup(SignUpRequest request) {

        if(userRepository.existsByEmail(request.getEmail())) {
            throw new IllegalStateException("Email already taken");
        }
   /*     User userParent = userRepository.findById(request.getDependOnUser().getId())
                .orElseThrow(() -> new RuntimeException("User not found"));*/


        var userBuilder = UserEntity.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .email(request.getEmail())
                .phoneNumber(request.getPhoneNumber())
                .address(request.getAddress())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.valueOf(request.getRole()));

        if (request.getDependOnUserEntity() != null && StringUtils.isNotBlank(StringUtils.trim(String.valueOf(request.getDependOnUserEntity())))) {
            userBuilder.dependOnUserEntity(request.getDependOnUserEntity());
        }

        var user = userBuilder.build();
        user = userService.save(user);
        var jwt = jwtService.generateToken(user);
        return JwtAuthenticationResponse.builder().token(jwt).build();
    }
    public JwtAuthenticationResponse signin(SignInRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
        var user = userRepository.findByEmail(request.getEmail())
                .orElseThrow(() -> new IllegalArgumentException("Invalid email or password."));
        log.debug("JWT - {}", user.getEmail());

        // Update last login date
        userService.updateLastLoginDate(user);

        var jwt = jwtService.generateToken(user);
        return JwtAuthenticationResponse
                .builder()
                .token(jwt)
                .build();
    }

}