package com.transfert.electronicwallet.services;

import com.transfert.electronicwallet.domain.entities.CurrencyEntity;
import com.transfert.electronicwallet.domain.entities.ExchangeRateCurrencyEntity;
import com.transfert.electronicwallet.services.impl.ExchangeRateCurrencyImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
* @author diakhabyboubacar
* @email th.boubacar.diakhaby@gmail.com
* @since 13/01/2024
*/
@Service
public interface ExchangeRateCurrencyService {

    boolean save(ExchangeRateCurrencyEntity exchangeRateCurrency);

    Optional<ExchangeRateCurrencyEntity> findById(String id);

    Boolean existsById(String id);

    Page<ExchangeRateCurrencyEntity> findAll(Pageable pageable);

    boolean update(String id,ExchangeRateCurrencyEntity exchangeRateCurrency);

    Optional<ExchangeRateCurrencyEntity> findRate(CurrencyEntity fromCurrency, CurrencyEntity toCurrency);
}
