package com.transfert.electronicwallet.services;

import com.transfert.electronicwallet.domain.entities.AgencyEntity;
import com.transfert.electronicwallet.domain.entities.TransferEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 20/02/2024
 */
public interface TransferService {

    Page<TransferEntity> findAll(Pageable pageable);

    Page<TransferEntity> findByAgencySender(AgencyEntity agencySender, Pageable pageable);

    Page<TransferEntity> findByAgencyReceiver(AgencyEntity agencyReceiver, Pageable pageable);

    Page<TransferEntity> findByAgencySenderAndAgencyReceiver(String agencySender, String agencyReceiver, Pageable pageable);

    Page<TransferEntity> findByClient(String client,Pageable pageable);

    Page<TransferEntity> findByClientAndStatus(String client, String status, Pageable pageable);

    Page<TransferEntity> findByStatus(String status, Pageable pageable);

    Page<TransferEntity> findByClientAndType(String client, String type, Pageable pageable);

    Page<TransferEntity> findByType(String type, Pageable pageable);

    boolean existsById(String id);

    boolean existsByCode(String code);

    boolean save(TransferEntity transfer);

    TransferEntity findById(String id);

    TransferEntity findByCode(String code);

    boolean update(String id,TransferEntity transfer);

    boolean pay(TransferEntity transfer);

    boolean cancel(TransferEntity transfer);

    boolean deleteById(String id);
}
