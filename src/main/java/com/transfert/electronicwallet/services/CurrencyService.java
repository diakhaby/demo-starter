package com.transfert.electronicwallet.services;

import com.transfert.electronicwallet.domain.entities.CurrencyEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 11/01/2024
 */

@Service
public interface CurrencyService {

    Page<CurrencyEntity> findAll(Pageable pageable);

    CurrencyEntity save(CurrencyEntity currencyEntity);

    CurrencyEntity update(String id,CurrencyEntity currencyEntity);

   Optional<CurrencyEntity> findById(String id);

    boolean existsById(String id);

    boolean deleteById(String id);


}
