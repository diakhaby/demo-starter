package com.transfert.electronicwallet.services;

import com.transfert.electronicwallet.domain.entities.UserEntity;
import com.transfert.electronicwallet.repositories.UserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

import static com.transfert.electronicwallet.constant.Constant.PHOTO_DIRECTORY;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Service
@Transactional(rollbackOn = Exception.class)
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    public UserDetailsService userDetailsService(){
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                return userRepository.findByEmail(username)
                        .orElseThrow(() -> new UsernameNotFoundException("User not found"));

            }
        };
    }

    public UserEntity save(UserEntity newUserEntity){

        if(newUserEntity.getId() == null) {
            newUserEntity.setCreatedAt(LocalDateTime.now());
        }
        newUserEntity.setUpdatedAt(LocalDateTime.now());
        return userRepository.save(newUserEntity);
    }

    public void updateLastLoginDate(UserEntity userEntity) {
        userEntity.setLastLogin(LocalDateTime.now());
        userRepository.save(userEntity);
    }

    public UserEntity updateUser(UserEntity userEntity){
        userEntity.setPassword(passwordEncoder.encode(userEntity.getPassword()));
        return userRepository.save(userEntity);
    }
    public Page<UserEntity>getAllUsers(int page, int size){
        return userRepository.findAll(PageRequest.of(page, size, Sort.by("createdAt").descending()));
    }

    public UserEntity getUserById(String id){

        return userRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("User not found"));

    }

    public String uploadPhoto(String id, MultipartFile file){
        log.info("Saving photo for user ID: {}", id);

       UserEntity userEntity = getUserById(id);
       String photoUrl = photoFunction.apply(id, file);
       userEntity.setPhotoUrl(photoUrl);
       userRepository.save(userEntity);
       return photoUrl;
    }


    private final Function<String, String> fileExtension = fileName -> Optional.of(fileName)
            .filter(name -> name.contains("."))
            .map(name -> "."  +name.substring(fileName.lastIndexOf(".") + 1))
            .orElse(".png");
    private final BiFunction<String, MultipartFile, String> photoFunction = (id, image) -> {

        String fileName = id + fileExtension.apply(image.getOriginalFilename());
        try {

            Path fileStorageLocation = Paths.get(PHOTO_DIRECTORY).toAbsolutePath().normalize();
            if(!Files.exists(fileStorageLocation)) {
                Files.createDirectory(fileStorageLocation);
            }

            Files.copy(image.getInputStream(), fileStorageLocation.resolve(fileName),REPLACE_EXISTING);

            return ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/api/v1/user/photo/" + id + fileExtension
                            .apply(image.getOriginalFilename()))
                    .toUriString();

        }catch (Exception e){
         throw new RuntimeException("Unable to upl oad file: " + e.getMessage());
        }
    };













}
