package com.transfert.electronicwallet.mappers;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 08/01/2024
 */
public interface Mapper<A,B> {

    B mapTo(A a);

    A mapFrom(B b);
}
