package com.transfert.electronicwallet.mappers;

import com.transfert.electronicwallet.config.Services;
import com.transfert.electronicwallet.domain.dto.AgencyBalanceDto;
import com.transfert.electronicwallet.domain.entities.AgencyBalanceEntity;
import com.transfert.electronicwallet.domain.entities.AgencyEntity;
import com.transfert.electronicwallet.domain.entities.CurrencyEntity;
import com.transfert.electronicwallet.domain.entities.UserEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 31/01/2024
 */

@Service
@Slf4j
public class AgencyBalanceMapper {

    public AgencyBalanceDto toAgencyBalanceDto(AgencyBalanceEntity agencyBalanceEntity) {
       AgencyBalanceDto agencyBalanceDto = new AgencyBalanceDto();
       agencyBalanceDto.setAgency(agencyBalanceEntity.getAgency().getId());
       agencyBalanceDto.setCurrency(agencyBalanceEntity.getCurrency().getId());
       agencyBalanceDto.setAmount(agencyBalanceEntity.getAmount());
       return agencyBalanceDto;
    }

    public AgencyBalanceEntity toAgencyBalanceEntity(AgencyBalanceDto agencyBalanceDto) {

        UserEntity userEntity = Services.userLogin();
        assert userEntity != null;
        AgencyBalanceEntity agencyBalanceEntity = new AgencyBalanceEntity();
        CurrencyEntity currencyEntity = new CurrencyEntity();

        currencyEntity.setId(agencyBalanceDto.getCurrency());

        AgencyEntity agencyEntity = new AgencyEntity();

        agencyEntity.setId(agencyBalanceDto.getAgency());

        agencyBalanceEntity.setCurrency(currencyEntity);
        agencyBalanceEntity.setAgency(agencyEntity);
        agencyBalanceEntity.setUser(userEntity);
        agencyBalanceEntity.setAmount(agencyBalanceDto.getAmount());

        return agencyBalanceEntity;
    }
}
