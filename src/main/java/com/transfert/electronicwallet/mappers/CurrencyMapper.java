package com.transfert.electronicwallet.mappers;

import com.transfert.electronicwallet.config.Services;
import com.transfert.electronicwallet.domain.dto.CurrencyDto;
import com.transfert.electronicwallet.domain.entities.CurrencyEntity;
import com.transfert.electronicwallet.domain.entities.UserEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 24/01/2024
 */
@Service
@Slf4j
public class CurrencyMapper {

    public CurrencyEntity toEntity(CurrencyDto currencyDto) {
        var userEntity = Services.userLogin();
        assert userEntity != null;
        //log.debug("user id : " + userEntity.getId());
        CurrencyEntity currencyEntity = new CurrencyEntity();
        currencyEntity.setName(currencyDto.getName());
        currencyEntity.setIso(currencyDto.getIso());
        currencyEntity.setCountryCode(currencyDto.getCountryCode());
        currencyEntity.setUser(userEntity);

        return currencyEntity;
    }

    public CurrencyDto toDto(CurrencyEntity currencyEntity) {
        return new CurrencyDto(
                currencyEntity.getName(),
                currencyEntity.getIso(),
                currencyEntity.getCountryCode()
        );
    }
}
