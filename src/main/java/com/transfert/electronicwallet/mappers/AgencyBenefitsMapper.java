package com.transfert.electronicwallet.mappers;

import com.transfert.electronicwallet.config.Services;
import com.transfert.electronicwallet.domain.dto.AgencyBenefitsDto;
import com.transfert.electronicwallet.domain.entities.AgencyBenefitsEntity;
import com.transfert.electronicwallet.domain.entities.AgencyEntity;
import com.transfert.electronicwallet.domain.entities.CurrencyEntity;
import com.transfert.electronicwallet.domain.entities.UserEntity;
import org.springframework.stereotype.Service;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 20/02/2024
 */
@Service
public class AgencyBenefitsMapper {

    public AgencyBenefitsEntity toAgencyBenefitsEntity(AgencyBenefitsDto agencyBenefitsDto) {
        UserEntity userEntity = Services.userLogin();
        assert userEntity != null;

        AgencyBenefitsEntity agencyBenefitsEntity = new AgencyBenefitsEntity();

        agencyBenefitsEntity.setAmount(agencyBenefitsDto.getAmount());

        AgencyEntity agencyEntity = new AgencyEntity();
        agencyEntity.setId(agencyBenefitsDto.getAgency());

        CurrencyEntity currencyEntity = new CurrencyEntity();
        currencyEntity.setId(agencyBenefitsDto.getCurrency());

        agencyBenefitsEntity.setAgency(agencyEntity);
        agencyBenefitsEntity.setCurrency(currencyEntity);
        agencyBenefitsEntity.setUser(userEntity);
        agencyBenefitsEntity.setUpdatedBy(userEntity);

        return agencyBenefitsEntity;
    }

    public AgencyBenefitsDto toAgencyBenefitsDto(AgencyBenefitsEntity agencyBenefitsEntity) {

        AgencyBenefitsDto agencyBenefitsDto = new AgencyBenefitsDto();
        agencyBenefitsDto.setAgency(agencyBenefitsEntity.getAgency().getId());
        agencyBenefitsDto.setCurrency(agencyBenefitsEntity.getCurrency().getId());
        agencyBenefitsDto.setAmount(agencyBenefitsEntity.getAmount());

        return agencyBenefitsDto;
    }
}
