package com.transfert.electronicwallet.mappers.impl;

import com.transfert.electronicwallet.domain.dto.ExchangeRateCurrencyDto;
import com.transfert.electronicwallet.domain.entities.ExchangeRateCurrencyEntity;
import com.transfert.electronicwallet.mappers.Mapper;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 13/01/2024
 */
@Component
public class ExchangeRateCurrencyMapperImpl implements Mapper<ExchangeRateCurrencyEntity, ExchangeRateCurrencyDto> {

    private final ModelMapper modelMapper;

    public ExchangeRateCurrencyMapperImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public ExchangeRateCurrencyDto mapTo(ExchangeRateCurrencyEntity exchangeRateCurrencyEntity) {
        return modelMapper.map(exchangeRateCurrencyEntity, ExchangeRateCurrencyDto.class);
    }

    @Override
    public ExchangeRateCurrencyEntity mapFrom(ExchangeRateCurrencyDto exchangeRateCurrencyDto) {
        return modelMapper.map(exchangeRateCurrencyDto, ExchangeRateCurrencyEntity.class);
    }


}
