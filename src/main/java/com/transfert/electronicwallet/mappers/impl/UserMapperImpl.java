package com.transfert.electronicwallet.mappers.impl;

import com.transfert.electronicwallet.domain.dto.UserDto;
import com.transfert.electronicwallet.domain.entities.UserEntity;
import com.transfert.electronicwallet.mappers.Mapper;
import org.modelmapper.ModelMapper;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 10/01/2024
 */
public class UserMapperImpl implements Mapper<UserEntity, UserDto> {

    private ModelMapper modelMapper;

    public UserMapperImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public UserDto mapTo(UserEntity userEntity) {
        return modelMapper.map(userEntity, UserDto.class);
    }

    @Override
    public UserEntity mapFrom(UserDto userDto) {
        return modelMapper.map(userDto, UserEntity.class);
    }
}
