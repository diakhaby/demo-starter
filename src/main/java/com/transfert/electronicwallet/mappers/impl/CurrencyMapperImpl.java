package com.transfert.electronicwallet.mappers.impl;

import com.transfert.electronicwallet.domain.dto.CurrencyDto;
import com.transfert.electronicwallet.domain.entities.CurrencyEntity;
import com.transfert.electronicwallet.mappers.Mapper;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 11/01/2024
 */
@Component
public class CurrencyMapperImpl implements Mapper<CurrencyEntity, CurrencyDto> {

    private final ModelMapper modelMapper;

    public CurrencyMapperImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public CurrencyDto mapTo(CurrencyEntity currencyEntity) {
        return modelMapper.map(currencyEntity, CurrencyDto.class);
    }

    @Override
    public CurrencyEntity mapFrom(CurrencyDto currencyDto) {
        return modelMapper.map(currencyDto, CurrencyEntity.class);
    }
}
