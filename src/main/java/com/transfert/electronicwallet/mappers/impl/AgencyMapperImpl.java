package com.transfert.electronicwallet.mappers.impl;

import com.transfert.electronicwallet.domain.dto.AgencyDto;
import com.transfert.electronicwallet.domain.entities.AgencyEntity;
import com.transfert.electronicwallet.mappers.Mapper;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 08/01/2024
 */
@Component
public class AgencyMapperImpl implements Mapper<AgencyEntity, AgencyDto> {

    private final ModelMapper modelMapper;

    public AgencyMapperImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public AgencyDto mapTo(AgencyEntity agencyEntity) {
        return modelMapper.map(agencyEntity, AgencyDto.class);
    }

    @Override
    public AgencyEntity mapFrom(AgencyDto agencyDto) {
        return modelMapper.map(agencyDto, AgencyEntity.class);
    }
}
