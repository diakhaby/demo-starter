package com.transfert.electronicwallet.mappers;

import com.transfert.electronicwallet.config.Services;
import com.transfert.electronicwallet.domain.dto.TransactionFeeDto;
import com.transfert.electronicwallet.domain.entities.AgencyEntity;
import com.transfert.electronicwallet.domain.entities.FeeEntity;
import com.transfert.electronicwallet.domain.entities.UserEntity;
import com.transfert.electronicwallet.services.AgencyService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 28/01/2024
 */
@Service
@AllArgsConstructor
@Slf4j
public class TransactionFeeMapper {
    
    private final AgencyService agencyService;

   public FeeEntity toTransactionFeeEntity(TransactionFeeDto transactionFeeDto){

       Optional<AgencyEntity> agencyEntityOptional = agencyService.findById(transactionFeeDto.getAgency());

       UserEntity userEntity = Services.userLogin();
       assert userEntity != null;

           if(agencyEntityOptional.isPresent()) {
               AgencyEntity agEntity = agencyEntityOptional.get();

               FeeEntity entity = new FeeEntity();
               entity.setAgency(agEntity);
               entity.setMinAmount(transactionFeeDto.getMinAmount());
               entity.setMaxAmount(transactionFeeDto.getMaxAmount());
               entity.setFixedFee(transactionFeeDto.getFixedFee());
               entity.setPercentFee(transactionFeeDto.getPercentFee());
               entity.setUser(userEntity);
               return entity;

       }else {
           throw new RuntimeException("Agency not found");
       }
   }


    public TransactionFeeDto toTransactionFeeDto(FeeEntity feeEntity){

        TransactionFeeDto dto = new TransactionFeeDto();
        dto.setMinAmount(feeEntity.getMinAmount());
        dto.setMaxAmount(feeEntity.getMaxAmount());
        dto.setFixedFee(feeEntity.getFixedFee());
        dto.setPercentFee(feeEntity.getPercentFee());
        dto.setAgency(feeEntity.getAgency().getId());
        return dto;
    }
}
