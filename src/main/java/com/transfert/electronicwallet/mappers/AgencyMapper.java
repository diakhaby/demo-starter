package com.transfert.electronicwallet.mappers;

import com.transfert.electronicwallet.config.Services;
import com.transfert.electronicwallet.domain.dto.AgencyDto;
import com.transfert.electronicwallet.domain.entities.AgencyEntity;
import org.springframework.stereotype.Service;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 29/01/2024
 */
@Service
public class AgencyMapper {

    public AgencyEntity toEntity(AgencyDto agencyDto) {
        var userEntity = Services.userLogin();
        assert userEntity != null;
        var AgencyEntity = new AgencyEntity();
        AgencyEntity.setName(agencyDto.getName());
        AgencyEntity.setEmail(agencyDto.getEmail());
        AgencyEntity.setPhoneNumber(agencyDto.getPhoneNumber());
        AgencyEntity.setAddress(agencyDto.getAddress());
        AgencyEntity.setUser(userEntity);
        return AgencyEntity;
    }

    public AgencyDto toDto(AgencyEntity agencyEntity) {
       AgencyDto agencyDto = new AgencyDto();
       agencyDto.setName(agencyEntity.getName());
       agencyDto.setEmail(agencyEntity.getEmail());
       agencyDto.setPhoneNumber(agencyEntity.getPhoneNumber());
       agencyDto.setUser(agencyEntity.getUser());

       return agencyDto;
    }
}
