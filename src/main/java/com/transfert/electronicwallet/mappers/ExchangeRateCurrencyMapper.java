package com.transfert.electronicwallet.mappers;

import com.transfert.electronicwallet.config.Services;
import com.transfert.electronicwallet.domain.dto.ExchangeRateCurrencyDto;
import com.transfert.electronicwallet.domain.entities.CurrencyEntity;
import com.transfert.electronicwallet.domain.entities.ExchangeRateCurrencyEntity;
import com.transfert.electronicwallet.domain.entities.UserEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.User;
import org.springframework.stereotype.Service;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 22/01/2024
 */
@Service
@Slf4j
public class ExchangeRateCurrencyMapper {

    public ExchangeRateCurrencyEntity toExchangeRateCurrencyEntity(ExchangeRateCurrencyDto exchangeRateCurrencyDto) {

        if (exchangeRateCurrencyDto == null ) {
            throw new NullPointerException(" The DTO is null");
        }


        UserEntity userEntity = Services.userLogin();
        assert userEntity != null;

         ExchangeRateCurrencyEntity  exchangeRateCurrencyEntity = new ExchangeRateCurrencyEntity();
        exchangeRateCurrencyEntity.setRate(exchangeRateCurrencyDto.getRate());

        CurrencyEntity currencyEntityFrom = new CurrencyEntity();
        currencyEntityFrom.setId(exchangeRateCurrencyDto.getCurrencyFrom());

        CurrencyEntity currencyEntityTo = new CurrencyEntity();

        currencyEntityTo.setId(exchangeRateCurrencyDto.getCurrencyFrom());

        exchangeRateCurrencyEntity.setCurrencyFrom(currencyEntityFrom);
        exchangeRateCurrencyEntity.setCurrencyTo(currencyEntityTo);
        exchangeRateCurrencyEntity.setUser(userEntity);
        exchangeRateCurrencyEntity.setCreatedby(userEntity);

        return exchangeRateCurrencyEntity;

    }

    public ExchangeRateCurrencyDto toEntity(ExchangeRateCurrencyEntity exchangeRateCurrency){
        if(exchangeRateCurrency == null) {
            throw new NullPointerException(" The exchange rate currency is null");
        }
        var userEntity = Services.userLogin();
        assert userEntity != null;
        log.info("user id : " + userEntity.getId());
        log.info("the rate is : " + exchangeRateCurrency.getRate());
        return new ExchangeRateCurrencyDto(
                exchangeRateCurrency.getRate(),
                exchangeRateCurrency.getCurrencyTo().getId(),
                exchangeRateCurrency.getCurrencyFrom().getId()
        );
    }
}
