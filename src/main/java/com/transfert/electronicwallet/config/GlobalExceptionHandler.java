package com.transfert.electronicwallet.config;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 24/01/2024
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<String> handleDataIntegrityViolation(DataIntegrityViolationException ex) {
        // Vérifiez si l'exception est due à une violation de contrainte d'unicité
        if (ex.getCause() instanceof org.hibernate.exception.ConstraintViolationException) {
            String errorMessage = "Une combinaison similaire existe déjà dans la table.";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }

        // Si ce n'est pas une violation de contrainte d'unicité, renvoyez une réponse générique d'erreur
        return new ResponseEntity<>("Erreur lors de la manipulation des données.", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
