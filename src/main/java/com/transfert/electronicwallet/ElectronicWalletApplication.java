package com.transfert.electronicwallet;

import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElectronicWalletApplication {

	public static void main(String[] args) {
		loadDotenv();
		SpringApplication.run(ElectronicWalletApplication.class, args);
	}

	private static void loadDotenv() {
		Dotenv dotenv = Dotenv.load();
		System.setProperty("DATABASE_HOST", dotenv.get("MYSQL_HOST"));
		System.setProperty("DATABASE_PORT", dotenv.get("MYSQL_PORT"));
		System.setProperty("DATABASE_NAME", dotenv.get("MYSQL_DATABASE"));
		System.setProperty("DATABASE_USERNAME", dotenv.get("MYSQL_USER"));
		System.setProperty("DATABASE_PASSWORD", dotenv.get("MYSQL_PASSWORD"));
		System.setProperty("TOKEN_KEY", dotenv.get("TOKEN_KEY"));
		System.setProperty("TOKEN_EXPIRATION", dotenv.get("TOKEN_EXPIRATION"));
	}
}
