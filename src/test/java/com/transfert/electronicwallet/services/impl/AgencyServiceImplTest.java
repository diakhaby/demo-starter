package com.transfert.electronicwallet.services.impl;

import com.transfert.electronicwallet.domain.entities.AgencyEntity;
import com.transfert.electronicwallet.repositories.AgencyRepository;
import org.apache.coyote.BadRequestException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AgencyServiceImplTest {

   @Mock
   private AgencyRepository agencyRepository;

    private AgencyServiceImpl agencyService;

    @BeforeEach
    void setUp() {
        agencyService = new AgencyServiceImpl(agencyRepository);
    }

    @AfterEach
    void tearDown() {
        agencyRepository.deleteAll();
    }

    @Test
    public void givenAnAgencyWhenSaveAgencyThenReturnTrue() throws BadRequestException {

        // Given
        AgencyEntity agencyEntity = new AgencyEntity();
        agencyEntity.setName("Test Agency");
        when(agencyRepository.save(agencyEntity)).thenReturn(agencyEntity);

        // When
        boolean savedAgency = agencyService.save(agencyEntity);

        // Then
        assertThat(savedAgency).isTrue();
        verify(agencyRepository).findByName(agencyEntity.getName());
        verify(agencyRepository).save(agencyEntity);
    }

    @Test
    public void givenExistingAgencyWhenSaveThenShouldReturnFalse() throws BadRequestException {

        // Given
        AgencyEntity existingAgencyEntity = new AgencyEntity();
        existingAgencyEntity.setName("Test Existing Agency");
        when(agencyRepository.findByName(existingAgencyEntity.getName())).thenReturn(Optional.of(existingAgencyEntity));

        // When
        boolean savedAgency = agencyService.save(existingAgencyEntity);

        // Then
        assertThat(savedAgency).isFalse();
        verify(agencyRepository).findByName(existingAgencyEntity.getName());
        verify(agencyRepository, never()).save(any());
    }

    @Test
    void shouldFindAnAgencyById() {
    }

    @Test
    void shouldCheckIfAgencyExistsById() {
    }

    @Test
    void shouldFindAllAgencies() {
    }

    @Test
    void canFindAllAgencies() {
    }

    @Test
    void canMakePartialUpdate() {
    }

    @Test
    void canUpdate() {
    }

    @Test
    void canDeleteAgencyById() {
    }
}