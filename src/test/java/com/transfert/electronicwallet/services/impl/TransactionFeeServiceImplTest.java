package com.transfert.electronicwallet.services.impl;

import com.transfert.electronicwallet.repositories.TransactionFeeRepository;
import com.transfert.electronicwallet.services.TransactionFeeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TransactionFeeServiceImplTest {

    @Mock
    TransactionFeeRepository transactionFeeRepository;

    private TransactionFeeService transactionFeeService;

    @BeforeEach
    void setUp() {
        transactionFeeService = new TransactionFeeServiceImpl(transactionFeeRepository);
    }

    @Test
    void save() {



    }

    @Test
    void update() {
    }

    @Test
    void findById() {
    }

    @Test
    void findByAgencyId() {
    }

    @Test
    void findByAgencyIdAndMinAmountAndMaxAmount() {
    }

    @Test
    void findAll() {
    }

    @Test
    void deleteById() {
    }
}