package com.transfert.electronicwallet;

import com.transfert.electronicwallet.domain.entities.AgencyEntity;
import com.transfert.electronicwallet.repositories.AgencyRepository;

import java.util.Optional;

/**
 * @author diakhabyboubacar
 * @email th.boubacar.diakhaby@gmail.com
 * @since 05/02/2024
 */
public class Utils {

    AgencyRepository agencyRepository;

    public AgencyEntity newAgency() {
         AgencyEntity agencyEntity = new AgencyEntity();

         agencyEntity.setId("123");
         agencyEntity.setName("AG 1");
         agencyEntity.setAddress("184 BD Rue de la paix");
         agencyEntity.setPhoneNumber("06 00 00 00 00");
         agencyEntity.setEmail("lTqkG@example.com");

        return agencyEntity;
    }
}
