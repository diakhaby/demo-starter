import "./App.css";

import { useState } from "react";
import { Container, Col, Row } from "react-bootstrap";
import { Authentication } from "./components/user/Authentication.jsx";
import { request, setAuthToken } from "./api/axios_helper.js";
import Home from "./components/Home.jsx";
import Sidebar from "./components/common/Sidebar.jsx";
import Header from "./components/common/Header.jsx";
function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const authentication = (email, password) => {
    request("POST", "signin", {
      email: email,
      password: password,
    })
      .then((response) => {
        setAuthToken(response.data.token);
        setIsLoggedIn(true);
      })
      .catch((error) => {
        console.log(error.message);
      });
  };
  const handleLogin = () => {
    setIsLoggedIn(true);
  };
  const handleLogout = () => {
    setIsLoggedIn(setAuthToken(""));
  };
  const token = window.localStorage.getItem("auth_token");

  return (
    <>
      {!token ? (
        <Authentication authentication={authentication} />
      ) : (
        <div>
          <Header
            isLoggedIn={isLoggedIn}
            onLogin={handleLogin}
            onLogout={handleLogout}
          />

          <Container fluid className="main-container">
            <Row>
              <Sidebar />
              <Col
                md={10}
                className="content d-flex justify-content-center align-items-center"
              >
                {token && <Home />}
              </Col>
            </Row>
          </Container>
        </div>
      )}
      ;
    </>
  );
}

export default App;
