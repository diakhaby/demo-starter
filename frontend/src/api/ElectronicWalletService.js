import axios from "axios";

const API_URL = "http://localhost:8080/api/v1/";

// Save user
export async function saveUser(user, token) {
  const headers = {
    Authorization: `Bearer ${token}`,
  };

  return await axios.post(`${API_URL}signup`, user, { headers });
}

// get all users
export async function getUsers(page = 0, size = 10) {
  const token = window.localStorage.getItem("auth_token");
  const headers = {
    Authorization: `Bearer ${token}`,
  };

  // Effectuer la requête Axios avec l'en-tête d'autorisation
  return await axios.get(`${API_URL}user/admin?page=${page}&size=${size}`, {
    headers,
  });
  //console.log(response.data);
  // Retourner les données de la réponse
  //return response.data;
}

export async function getAllUsers(token) {
  const headers = {
    Authorization: `Bearer ${token}`,
  };
  const response = await axios.get(`${API_URL}user/admin`, { headers });
  // Retourner les données de la réponse
  return response;
}

// get user
export async function getUser(id, token) {
  const headers = {
    Authorization: `Bearer ${token}`,
  };
  return await axios.get(`${API_URL}user/${id}`, { headers });
}

// Save user
export async function updateUser(id, user, token) {
  const headers = {
    Authorization: `Bearer ${token}`,
  };

  return await axios.put(`${API_URL}user/update/${id}`, user, { headers });
}

// update user's photo
export async function updateUserPhoto(formData) {
  return await axios.put(`${API_URL}user/photo`, formData);
}

// update user's photo
export async function deleteUser(id) {
  return await axios.delete(`${API_URL}user/delete/${id}`);
}

export async function userLogged(token) {
  const headers = {
    Authorization: `Bearer ${token}`,
  };

  return await axios.get(`${API_URL}user/userLogged`, { headers });
}
