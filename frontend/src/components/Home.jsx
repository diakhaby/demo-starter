import React, { useEffect, useState } from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import { getUsers } from "../api/ElectronicWalletService.js";
import UserList from "./userEntity/UserList.jsx";
import UserDetail from "./userEntity/UserDetail.jsx";
import NewUser from "./NewUser";
import Dashboard from "../Dashboard.jsx";
import UserEditForm from "./userEntity/UserEditForm.jsx";

const Home = () => {
  const [data, setData] = useState({});
  const [currentPage, setCurrentPage] = useState(0);

  const getAllUsers = async (page = 0, size = 10) => {
    try {
      // Utiliser authHeader directement ici
      setCurrentPage(page); // Mise à zéro de la page car cela semble être un appel initial
      const { data } = await getUsers(page, size);
      console.log(data);
      setData(data);
    } catch (error) {
      // console.log(error.message);
    }
  };

  useEffect(() => {
    getAllUsers();
  }, []);

  return (
    <div className="col my-5">
      <div className="my-5"></div>
      <Routes>
        <Route path="/" element={<Navigate to={"/dashboard"} />} />
        <Route path="/dashboard" element={<Dashboard />} />
        <Route
          path="/users"
          element={
            <UserList
              data={data}
              currentPage={currentPage}
              getAllUsers={getAllUsers}
            />
          }
        />
        <Route path="/users/:id" element={<UserDetail />} />
        <Route path="/update/userEntity/:id" element={<UserEditForm />} />
        <Route path="/utilisateur/nouveau" element={<NewUser />} />
      </Routes>
    </div>
  );
};

export default Home;
