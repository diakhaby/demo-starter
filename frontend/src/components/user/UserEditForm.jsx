import React, { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import {
  getUser,
  updateUser,
  getAllUsers,
} from "../../api/ElectronicWalletService.js";

import Input from "../form/Input";

const UserEditForm = () => {
  const navigate = useNavigate();
  const token = window.localStorage.getItem("auth_token");
  const [users, setUsers] = useState([]);
  const [dependOnUser, setDependOnUser] = useState("");
  const [user, setUser] = useState({
    firstName: "",
    lastName: "",
    //role: "",
    email: "",
    address: "",
    phoneNumber: "",
    //dependOnUser: "",
  });
  const { id } = useParams();

  const fetchUser = async (id) => {
    let token = window.localStorage.getItem("auth_token");
    try {
      const { data } = await getUser(id, token);
      setUser(data);
    } catch (error) {}
  };

  const handleChange = (event) => {
    setUser({ ...user, [event.target.name]: event.target.value });
  };

  const handleUpdate = async (e) => {
    const token = window.localStorage.getItem("auth_token");
    e.preventDefault();
    setDependOnUser(user.dependOnUser);
    try {
      // Déclarez et initialisez userData avant de l'utiliser
      const userData = cleanUser(user);
      console.log(userData);
      await updateUser(id, userData, token);

      setDependOnUser("");
      setUser({
        firstName: "",
        lastName: "",
        role: "",
        email: "",
        address: "",
        phoneNumber: "",
        dependOnUser: "",
      });
      navigate("/users");
    } catch (error) {
      console.log(error.message);
    }
  };

  const fetchAllUsers = async () => {
    try {
      const { data } = await getAllUsers(token);
      setUsers(data);
    } catch (error) {}
  };

  useEffect(() => {
    fetchAllUsers();
    fetchUser(id);
  }, [id]);

  const cleanUser = ({
    createdAt,
    updatedAt,
    password,
    role,
    authorities,
    dependOnUser,
    ...rest
  }) => rest;
  return (
    <div className="card">
      <div className="card-header">
        <div className="card-title">Nouvelle utilisateur</div>
      </div>
      <div className="card-body">
        <form onSubmit={handleUpdate}>
          <input type="hidden" name="id" value={user.id} />
          <div className="form-group row">
            <div className="col-md-12">
              <div className="form-floating mb-3">
                <select
                  name="dependOnUser"
                  className="form-control"
                  value={user.dependOnUser || ""}
                  onChange={handleChange}
                  id="dependOnUser"
                >
                  <option value="null">Sélectionner le parent</option>
                  {users.content &&
                    users.content.map((user) => (
                      <option key={user.id} value={user.id}>
                        {user.firstName} {user.lastName}
                      </option>
                    ))}
                </select>
              </div>
            </div>
          </div>
          <div className="form-group row">
            <div className="col-md-6">
              <Input
                type="email"
                name="email"
                className="form-control"
                placeholder=""
                id="email"
                autoComplete="off"
                autoCorrect="off"
                value={user.email || ""}
                onChange={handleChange}
                label="Email"
              />
            </div>
            <div className="col-md-6">
              <div className="form-floating mb-3">
                <select
                  name="role"
                  className="form-control"
                  value={user.role || ""}
                  onChange={handleChange}
                  id="role"
                >
                  <option>Sélectionner le rôle</option>
                  <option value="ROLE_USER">Utilisateur</option>
                  <option value="ROLE_ADMIN">Administrateur</option>
                </select>
              </div>
            </div>
          </div>
          <div className="form-group row">
            <div className="col-md-6">
              <Input
                type="text"
                onChange={handleChange}
                className="form-control"
                placeholder=""
                id="firstName"
                autoComplete="off"
                autoCorrect="off"
                name="firstName"
                value={user.firstName || ""}
                label="First Name"
              />
            </div>
            <div className="col-md-6">
              <Input
                type="text"
                name="lastName"
                className="form-control"
                placeholder=""
                id="lastName"
                autoComplete="off"
                autoCorrect="off"
                value={user.lastName || ""}
                onChange={handleChange}
                label="Last Name"
              />
            </div>
          </div>

          <div className="form-group row">
            <div className="col-md-6">
              <Input
                type="text"
                onChange={handleChange}
                className="form-control"
                placeholder=""
                id="address"
                autoComplete="off"
                autoCorrect="off"
                name="address"
                value={user.address || ""}
                label="Adresse"
              />
            </div>
            <div className="col-md-6">
              <Input
                type="text"
                name="phoneNumber"
                className="form-control"
                placeholder=""
                id="phoneNumber"
                autoComplete="off"
                autoCorrect="off"
                value={user.phoneNumber || ""}
                onChange={handleChange}
                label="Téléphone"
              />
            </div>
          </div>

          <div className="float-end">
            <button type="submit" className="btn btn-sm btn-primary">
              Submit
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default UserEditForm;
