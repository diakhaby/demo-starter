import React, { useState } from "react";

export const Authentication = ({ authentication }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    try {
      authentication(email, password);
    } catch (error) {
      console.log("Erreur de connexion" + error.message);
    }
  };
  return (
    <div className="login template d-flex justify-content-center align-items-center vh-100 bg-primary">
      <div className="form_container p-5 rounded bg-white">
        <form action="submit" onSubmit={handleSubmit}>
          <h3 className="text-center"> Authentification</h3>
          <div className="form-floating mb-3">
            <input
              type="text"
              name="email"
              value={email}
              className="form-control"
              placeholder=""
              id="email"
              autoComplete="of"
              autoCorrect="of"
              onChange={(e) => {
                setEmail(e.target.value);
              }}
            />
            <label className="form-label" htmlFor="email">
              Email
            </label>
          </div>

          <div className="form-floating mb-3">
            <input
              type="password"
              name="password"
              value={password}
              className="form-control"
              placeholder=""
              id="password"
              autoComplete="of"
              autoCorrect="of"
              onChange={(e) => {
                setPassword(e.target.value);
              }}
            />
            <label className="form-label" htmlFor="password">
              Mot de passe
            </label>
          </div>
          <div className="d-grid">
            <button className="btn btn-sm btn-primary">Sign in</button>
          </div>
        </form>
      </div>
    </div>
  );
};
