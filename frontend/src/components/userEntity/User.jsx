import React from "react";
import {
  Envelope,
  GeoAltFill,
  Pencil,
  Person,
  Phone,
} from "react-bootstrap-icons";
import { Link } from "react-router-dom";
import CockPit from "../../library/CockPit";

const User = ({ user }) => {
  return (
    <div className="col-md-4">
      <div className="card mb-4">
        <div className="card-body shadow bg-green">
          <div className="row">
            <div className="col-md-4">
              {user.photoUrl && (
                <img src={user.photoUrl} alt={user.firstName} />
              )}
              <img
                src="https://api.dicebear.com/7.x/personas/svg"
                className="img-fluid rounded-circle shadow"
                alt="avatar"
                style={{ width: "75px" }}
              />
              <p className="ms-3">{CockPit.displayRole(user.role)}</p>
              {user.lastLogin && (
                <p className="user_title">
                  Dernière connexion
                  {CockPit.formatDateInFrench(user.lastLogin)}{" "}
                </p>
              )}
            </div>
            <div className="col-md-8">
              <div className="row">
                <div className="col-md-9">
                  <Link
                    to={`/users/${user.id}`}
                    className="text-decoration-none text-muted fw-bold"
                  >
                    <p>
                      <Person />{" "}
                      <span className="me-2">
                        {user.firstName.substring(0, 15)}
                      </span>
                      {user.lastName.substring(0, 15)}
                    </p>
                  </Link>
                </div>
                <div className="col-md-3">
                  <Link
                    to={`/update/user/${user.id}`}
                    className="btn btn-sm btn-warning fload-end"
                  >
                    <Pencil /> Edit
                  </Link>
                </div>
              </div>

              <p>
                <a
                  href={`tel:${user.email}`}
                  className="me-2 text-decoration-none text-muted"
                >
                  <Envelope /> {user.email}
                </a>
                {user.phoneNumber && (
                  <a
                    href={`tel:${user.phoneNumber}`}
                    className="text-decoration-none text-muted"
                  >
                    <Phone /> {user.phoneNumber}
                  </a>
                )}
              </p>
              {user.address && (
                <p>
                  <GeoAltFill />
                  {user.address ? user.address.substring(0, 20) : ""}
                </p>
              )}
            </div>
          </div>
          <div className="card border-none">
            {user.dependOnUser && (
              <div className="card-body shadow rounded border-none p-2">
                <p className="fw-bold">Utilisateur parent</p>
                <div className="row">
                  <div className="col-md-3">
                    {user.dependOnUser.photoUrl && (
                      <img
                        src={user.dependOnUser.photoUrl}
                        alt={user.dependOnUser.firstName}
                      />
                    )}
                    <img
                      src="https://api.dicebear.com/7.x/avataaars/svg"
                      className="img-fluid rounded-circle shadow"
                      alt="avatar"
                      style={{ width: "75px" }}
                    />
                  </div>
                  <div className="col-md-9">
                    <p>
                      <Link
                        to={`/users/${user.dependOnUser.id}`}
                        className="text-decoration-none text-dark fw-bold"
                      >
                        {user.dependOnUser.firstName}{" "}
                        {user.dependOnUser.lastName}
                      </Link>
                    </p>
                    <p>
                      <a
                        href={`tel:${user.dependOnUser.email}`}
                        className="me-2 text-decoration-none text-muted"
                      >
                        <Envelope /> {user.dependOnUser.email}
                      </a>
                      {user.dependOnUser.phoneNumber && (
                        <a
                          href={`tel:${user.dependOnUser.phoneNumber}`}
                          className="text-decoration-none text-muted"
                        >
                          <Phone /> {user.dependOnUser.phoneNumber}
                        </a>
                      )}
                    </p>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
export default User;
