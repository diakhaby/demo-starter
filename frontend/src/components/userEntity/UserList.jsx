import React from "react";
import { Link } from "react-router-dom";
import { PersonPlus, PlusCircle } from "react-bootstrap-icons";
import User from "./User.jsx";

const UserList = ({ data, currentPage, getAllUsers }) => {
  return (
    <div className="row my-5 justify-content-center align-items-center">
      <Link to="/utilisateur/nouveau" className="mb-2 text-decoration-none">
        <PersonPlus className="fs-5" /> Nouvel utilisateur
      </Link>

      {data?.content?.length === 0 && <div>Pas d'utilisateur</div>}

      {data?.content?.length > 0 &&
        data.content.map((user) => <User user={user} key={user.id} />)}

      {data?.content?.length > 0 && data?.totalPages > 1 && (
        <div className="d-flex justify-content-center align-items-center">
          <nav className="Page navigation example">
            <ul className="pagination">
              <li
                className={`page-item cursor-pointer ${
                  0 === currentPage ? "disabled" : ""
                }`}
              >
                <a
                  onClick={() => getAllUsers(currentPage - 1)}
                  className="page-link "
                >
                  &laquo;
                </a>
              </li>

              {data &&
                [...Array(data.totalPages).keys()].map((page, index) => (
                  <li
                    key={index}
                    className={`page-item cursor-pointer ${
                      currentPage === page ? "active" : ""
                    }`}
                  >
                    <a
                      onClick={() => getAllUsers(page)}
                      className="page-link "
                      key={page}
                    >
                      {page + 1}
                    </a>
                  </li>
                ))}
              <li
                className={`page-item cursor-pointer ${
                  data.totalPages === currentPage + 1 ? "disabled" : ""
                }`}
              >
                <a
                  onClick={() => getAllUsers(currentPage + 1)}
                  className="page-link"
                >
                  &raquo;
                </a>
              </li>
            </ul>
          </nav>
        </div>
      )}
    </div>
  );
};

export default UserList;
