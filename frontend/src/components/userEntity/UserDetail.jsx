import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { ArrowBarLeft } from "react-bootstrap-icons";

import { getUser } from "../../api/ElectronicWalletService.js";

const UserDetail = () => {
  const [user, setUser] = useState([]);
  const { id } = useParams();

  const fetchUser = async (id) => {
    let token = window.localStorage.getItem("auth_token");
    try {
      const { data } = await getUser(id, token);
      setUser(data);
    } catch (error) {}
  };

  useEffect(() => {
    fetchUser(id);
  }, [id]);

  return (
    <div className="col d-flex flex-column h-sm-100 my-5">
      <main className="row overflow-auto">
        <Link to={"/users"}>
          <ArrowBarLeft />
          Afficher la liste
        </Link>
        <div className="col pt-4">
          <h3>{user.firstName}</h3>
          <p className="lead">
            <a href={`mailto:${user.email}`}>{user.email}</a> -{" "}
            {user.phoneNumber ? user.phoneNumber : "#"}
          </p>
          <hr />
          <h3>More content...</h3>
        </div>
      </main>
      <footer className="row bg-light py-4 mt-auto">
        <div className="col">
          Détails {user.firstName} {user.lastName}
        </div>
      </footer>
    </div>
  );
};

export default UserDetail;
