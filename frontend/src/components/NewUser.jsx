import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Input from "../components/form/Input";
import { getAllUsers, saveUser } from "../api/ElectronicWalletService";

const NewUser = () => {
  const navigate = useNavigate();
  const token = window.localStorage.getItem("auth_token");
  const [users, setUsers] = useState([]);
  const [dependOnUser, setDependOnUser] = useState("");
  const [values, setValues] = useState({
    firstName: "",
    lastName: "",
    role: "",
    email: "",
    password: "",
    address: "",
    phoneNumber: "",
    //dependOnUser: "",
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    const token = window.localStorage.getItem("auth_token");
    e.preventDefault();
    setDependOnUser(values.dependOnUser);
    try {
      const userData = { ...values };
      if (dependOnUser) {
        userData.dependOnUser = dependOnUser;
      }
      await saveUser(userData, token);
      setDependOnUser("");
      setValues({
        firstName: "",
        lastName: "",
        role: "",
        email: "",
        password: "",
        address: "",
        phoneNumber: "",
        dependOnUser: "",
      });
      navigate("/users");
    } catch (error) {
      console.log(error.message);
    }
  };

  const fetchAllUsers = async () => {
    try {
      const { data } = await getAllUsers(token);
      setUsers(data);
    } catch (error) {}
  };

  useEffect(() => {
    fetchAllUsers();
  }, []);

  return (
    <div className="card">
      <div className="card-header">
        <div className="card-title">Nouvelle utilisateur</div>
      </div>
      <div className="card-body">
        <form onSubmit={handleSubmit}>
          <div className="form-group row">
            <div className="col-md-12">
              <div className="form-floating mb-3">
                <select
                  name="dependOnUser"
                  className="form-control"
                  value={values.dependOnUser}
                  onChange={handleChange}
                  id="dependOnUser"
                >
                  <option value="null">Sélectionner le parent</option>
                  {users.content &&
                    users.content.map((user) => (
                      <option key={user.id} value={user.id}>
                        {user.firstName} {user.lastName}
                      </option>
                    ))}
                </select>
              </div>
            </div>
          </div>
          <div className="form-group row">
            <div className="col-md-4">
              <Input
                type="email"
                name="email"
                className="form-control"
                placeholder=""
                id="email"
                autoComplete="off"
                autoCorrect="off"
                value={values.email}
                onChange={handleChange}
                label="Email"
              />
            </div>
            <div className="col-md-4">
              <div className="form-floating mb-3">
                <select
                  name="role"
                  className="form-control"
                  value={values.role}
                  onChange={handleChange}
                  id="role"
                >
                  <option>Sélectionner le rôle</option>
                  <option value="ROLE_USER">Utilisateur</option>
                  <option value="ROLE_ADMIN">Administrateur</option>
                </select>
              </div>
            </div>
            <div className="col-md-4">
              <Input
                type="password"
                name="password"
                className="form-control"
                placeholder=""
                id="password"
                autoComplete="off"
                autoCorrect="off"
                value={values.password}
                onChange={handleChange}
                label="Mot de passe"
              />
            </div>
          </div>
          <div className="form-group row">
            <div className="col-md-6">
              <Input
                type="text"
                onChange={handleChange}
                className="form-control"
                placeholder=""
                id="firstName"
                autoComplete="off"
                autoCorrect="off"
                name="firstName"
                value={values.firstName}
                label="First Name"
              />
            </div>
            <div className="col-md-6">
              <Input
                type="text"
                name="lastName"
                className="form-control"
                placeholder=""
                id="lastName"
                autoComplete="off"
                autoCorrect="off"
                value={values.lastName}
                onChange={handleChange}
                label="Last Name"
              />
            </div>
          </div>

          <div className="form-group row">
            <div className="col-md-6">
              <Input
                type="text"
                onChange={handleChange}
                className="form-control"
                placeholder=""
                id="address"
                autoComplete="off"
                autoCorrect="off"
                name="address"
                value={values.address}
                label="Adresse"
              />
            </div>
            <div className="col-md-6">
              <Input
                type="text"
                name="phoneNumber"
                className="form-control"
                placeholder=""
                id="phoneNumber"
                autoComplete="off"
                autoCorrect="off"
                value={values.phoneNumber}
                onChange={handleChange}
                label="Téléphone"
              />
            </div>
          </div>

          <div className="float-end">
            <button type="submit" className="btn btn-sm btn-primary">
              Submit
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default NewUser;
