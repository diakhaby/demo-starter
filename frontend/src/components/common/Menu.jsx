// Menu.js
import React, { useState, useEffect } from "react";
import { Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import { userLogged } from "../../api/ElectronicWalletService";

const Menu = ({ onLogin, onLogout }) => {
  const token = window.localStorage.getItem("auth_token");
  const [currentUser, setCurrentUser] = useState([]);

  const getUserLogged = async () => {
    try {
      const { data } = await userLogged(token);
      setCurrentUser(data);
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    getUserLogged();
  }, []);

  //console.log(data);
  return (
    <Nav className="ms-auto">
      {token ? (
        <>
          <Nav.Item>
            <img
              src="https://api.dicebear.com/7.x/personas/svg"
              alt="User"
              className="userEntity-avatar img-fluid rounded-circle"
            />
          </Nav.Item>
          <Nav.Item onClick={onLogout} className="text-white cursor-pointer">
            <small>({currentUser.firstName})</small> Se déconnecter
          </Nav.Item>
        </>
      ) : (
        <Nav.Item onClick={onLogin} className="text-white">
          <Link
            to="/authentification"
            className="cursor-pointer text-white text-decoration-none"
          >
            Se connecter
          </Link>
        </Nav.Item>
      )}
    </Nav>
  );
};

export default Menu;
