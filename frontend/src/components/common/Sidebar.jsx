// Sidebar.js
import React from "react";
import { Col, Nav } from "react-bootstrap";
import { Link } from "react-router-dom"; // Vous pouvez utiliser ce lien pour la navigation entre les pages
import { People, House, PlusCircle } from "react-bootstrap-icons";

const Sidebar = () => {
  const token = window.localStorage.getItem("auth_token");

  return (
    <>
      {token && (
        <Col md={2} className="sidebar shadow">
          <Nav defaultActiveKey="/accueil" className="flex-column">
            <Nav.Item className="mt-5">
              <Nav.Link as={Link} to="/dashboard">
                <House /> Accueil
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link as={Link} to="/users">
                <People /> Utilisateur
              </Nav.Link>
              <Nav className="flex-column pl-3">
                <Nav.Item>
                  <Nav.Link as={Link} to="/users">
                    Liste utilisateurs
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link as={Link} to="/utilisateur/nouveau">
                    {" "}
                    <PlusCircle /> Nouvel utilisateur
                  </Nav.Link>
                </Nav.Item>
              </Nav>
            </Nav.Item>
          </Nav>
        </Col>
      )}
    </>
  );
};

export default Sidebar;
