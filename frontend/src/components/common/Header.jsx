// Header.js
import React from "react";
import { Navbar, Container } from "react-bootstrap";

import Menu from "./Menu.jsx";

const Header = ({ isLoggedIn, onLogin, onLogout }) => {
  return (
    <Navbar bg="dark" variant="dark" expand="lg" fixed="top">
      <Container fluid>
        <Navbar.Brand href="#home">Logo</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbar" />
        <Navbar.Collapse id="navbar">
          <Menu isLoggedIn={isLoggedIn} onLogin={onLogin} onLogout={onLogout} />
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Header;
