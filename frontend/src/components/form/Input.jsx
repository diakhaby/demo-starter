// Input.js
import React from 'react';

const Input = ({ type, name, value, className, placeholder, id, autoComplete, autoCorrect, onChange, label }) => {
  return (
    <div className="form-floating mb-3">
      <input
        type={type}
        name={name}
        value={value}
        className={className}
        placeholder={placeholder}
        id={id}
        autoComplete={autoComplete}
        autoCorrect={autoCorrect}
        onChange={onChange}
      />
      <label className="form-label" htmlFor={id}>
        {label}
      </label>
    </div>
  );
};

export default Input;
