class CockPit {
    static displayRole(role) {
        switch (role) {
          case 'ROLE_ADMIN':
            return 'ADMIN';
          case 'ROLE_USER':
            return 'USER';
          // Ajoutez d'autres cas selon vos besoins
          default:
            return 'UNKNOWN ROLE';
        }
      }
      static formatDateInFrench(lastLogin) {
        if (!lastLogin || isNaN(new Date(lastLogin))) {
            return 'Invalid Date';
          }
          const dateObject = new Date(lastLogin);
        const options = {
          year: 'numeric',
          month: 'long',
          day: 'numeric',
          hour: 'numeric',
          minute: 'numeric',
          second: 'numeric',
          timeZoneName: 'short',
        };
    
        const dateFormatter = new Intl.DateTimeFormat('fr-FR', options);
        return dateFormatter.format(dateObject);
      }
}
export default CockPit;